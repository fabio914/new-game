# GlitchXY (a.k.a NewGame) #

This is my attempt on iOS game development. I haven't updated this project recently, so I decided to make it open-source. You can read more about it here: http://fabio914.blogspot.com.br/2014/06/new-game.html

This game isn't even near its completion, though some parts (such as the OpenGL renderer, the OpenAL audio engine, and the virtual gamepad) should be useful for other projects. This project can also be used as a tutorial for these technologies.

You can reuse parts of this project on your own projects, but remember to give me credit for the parts you use. Feel free to contact me if you think it's necessary: fabio914 at gmail.com.

Please refrain from using the whole project, since I still intend to publish it some day.

### Features ###

* Pseudo-3D FPS style game (originally a raycaster)
* OpenGL ES 2.0
* GLSL
* OpenAL
* Written in Objective-C and C.
