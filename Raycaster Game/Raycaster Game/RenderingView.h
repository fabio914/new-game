//
//  RenderingView.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/22/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "General.h"
#include "Vectors.h"

#import "Player.h"
#import "Texture.h"
#import "BasicLevelController.h"

@interface RenderingView : UIView {
    
    @protected
    unsigned heartbeat;
}

@property (assign, nonatomic) Player * player;
@property (assign, nonatomic) BasicLevelController * level;

- (void)updateHeartbeat:(unsigned)h;
- (void)renderFrameWithHeartbeat:(unsigned)h;

/* Override */
- (void)prepareForRendering;
- (void)renderFrame;
- (void)clearImageCache;
- (void)unload;

@end
