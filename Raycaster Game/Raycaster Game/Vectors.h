//
//  Vectors.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/19/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#ifndef Raycaster_Game_Vectors_h
#define Raycaster_Game_Vectors_h

typedef struct {
    double v[3];
} Vector3;

Vector3 newVector(double, double, double);
Vector3 sumVector(Vector3, Vector3);
Vector3 subVector(Vector3, Vector3);
double dotVector(Vector3, Vector3);
Vector3 crossVector(Vector3, Vector3);
Vector3 scalarVector(double, Vector3);
Vector3 divScalarVector(Vector3, double);
double normVector(Vector3);
Vector3 normalizeVector(Vector3);
int equalVector(Vector3, Vector3);
void debugPrintVector(Vector3);

#ifdef __OBJC__
NSDictionary * dictionaryFromVector(Vector3 v);
Vector3 vectorFromDictionary(NSDictionary * dictionary);
#endif

#endif
