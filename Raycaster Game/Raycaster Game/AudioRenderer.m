//
//  AudioRenderer.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "AudioRenderer.h"

#include <OpenAL/al.h>

@implementation AudioRenderer

- (void)prepareForRendering {
    
}

- (void)renderAudio {
    
}

- (void)unload {
    
    _player = nil;
    _level = nil;
}

@end
