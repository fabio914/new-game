//
//  MapFloor.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "MapFloor.h"

@implementation MapFloor

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if(self = [super init]) {
        
        /* Must be a 64x64 image! */
        if(dictionary[@"texture"]) {
        
            _texture = [[AnimatedTexture animatedTextureFromDescription:dictionary[@"texture"]] retain];
        }
        
        if(dictionary[@"path"]) {
            
            _path = [[Path pathWithDescription:dictionary[@"path"]] retain];
        }
        
        _stepOverAction = [(NSString *)dictionary[@"action"] retain];
    }
    
    return self;
}

- (void)dealloc {
    
    [_texture release], _texture = nil;
    [_path release], _path = nil;
    [_stepOverAction release], _stepOverAction = nil;
    
    [super dealloc];
}

- (BOOL)hasTexture {
    
    return _texture != nil && [_texture hasImage];
}

- (BOOL)hasPath {
    
    return _path != nil && [_path hasPath];
}

- (BOOL)hasStepOverAction {
    
    return  _stepOverAction != nil;
}

@end
