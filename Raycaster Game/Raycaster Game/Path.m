//
//  Path.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/29/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "Path.h"

@interface Path()

@property (nonatomic) unsigned currentIndex;
@property (nonatomic) int direction;

@end

@implementation Path

+ (Path *)pathWithDescription:(NSDictionary *)dictionary {
    
    Path * path = nil;
    
    NSString * modeString = (NSString *)dictionary[@"mode"];
    PathMode mode = 0;
    
    if([modeString isEqualToString:@"loop"]) {
        
        mode = PathModeLoop;
    }
    
    else if([modeString isEqualToString:@"changing"]) {
        
        mode = PathModeChanging;
    }
    
    else if([modeString isEqualToString:@"once"]) {
        
        mode = PathModeOnce;
    }
    
    BOOL active = [dictionary[@"active"] boolValue];
    unsigned interval = [dictionary[@"interval"] unsignedIntValue];
    unsigned replacement = [dictionary[@"replacement"] unsignedIntValue];
    
    path = [[Path alloc] initWithCoordiantes:dictionary[@"coordinates"] mode:mode interval:interval active:active replacement:replacement];
    
    return [path autorelease];
}

- (id)initWithCoordiantes:(NSArray *)coordinates mode:(PathMode)mode interval:(unsigned)interval active:(BOOL)active replacement:(unsigned)replacement {
    
    if(self = [super init]) {
        
        /* 
         * Does not check if these coordinates are valid!
         * (if they are inside the map or if they don't overlap
         *  with anything else) 
         *
         * Note: The initial coordinate must be the same
         * as the position marked on the map!
         *
         */
        _coordinates = [coordinates retain];
        
        _mode = mode;
        _active = active;
        _interval = interval == 0 ? 1:interval;
        _currentIndex = 0;
        _direction = 1;
        
        /* Does not check if this replacement is a valid map object */
        _replacement = replacement;
    }
    
    return self;
}

- (void)dealloc {
    
    [_coordinates release], _coordinates = nil;
    
    [super dealloc];
}

- (BOOL)hasPath {
    
    /* A path needs at least two positions */
    return _coordinates != nil && [_coordinates count] > 1;
}

- (unsigned)currentX {
    
    return [(NSNumber *)_coordinates[_currentIndex][0] unsignedIntValue];
}

- (unsigned)currentY {
    
    return [(NSNumber *)_coordinates[_currentIndex][1] unsignedIntValue];
}

- (void)updateWithHeartbeat:(unsigned)heartbeat {
    
    if(_active && (heartbeat % _interval) == 0) {
        
        if(_mode == PathModeLoop) {
            
            _currentIndex = (_currentIndex + 1) % [_coordinates count];
        }
        
        else if(_mode == PathModeChanging) {
            
            if(_currentIndex == 0 && _direction == -1) {
                
                _direction = 1;
            }
            
            if(_currentIndex == [_coordinates count] - 1 && _direction == 1) {
                
                _direction = -1;
            }
            
            _currentIndex += _direction;
        }
        
        else if(_mode == PathModeOnce) {
            
            _currentIndex += 1;
            
            if(_currentIndex >= [_coordinates count] - 1) {
                
                _active = NO;
            }
        }
    }
}

@end
