//
//  IntroBLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/26/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "IntroBLevel.h"
#import "Game.h"

@interface IntroBLevel()

@property (nonatomic) BOOL first, second, third;

@end

@implementation IntroBLevel

- (GameAction)extendPath {
    
    if(!_first) {
        
        [[[self floorAt:18] texture] setCurrentTexture:1];
        [[[self floorAt:21] path] setActive:YES];
        _first = YES;
    }
}

- (GameAction)startMoving {
    
    if(!_second) {
        
        [[[self floorAt:19] texture] setCurrentTexture:1];
        [[[self floorAt:12] path] setActive:YES];
        [[[self floorAt:12] texture] setCurrentTexture:1];
        [[[self objectAt:4] path] setActive:YES];
        _second = YES;
    }
}

- (GameAction)activateTeleport {
    
    if(!_third) {
        
        [[[self floorAt:20] texture] setCurrentTexture:1];
        [[[self floorAt:22] texture] setCurrentTexture:1];
        [[[self floorAt:23] texture] setCurrentTexture:1];
        [[[self floorAt:24] texture] setCurrentTexture:1];
        [self replaceObjectOfKind:1 withKind:2];
        _third = YES;
    }
}

- (GameAction)killPlayer {
    
    [[Game shared] reloadLevel];
}

- (GameAction)nextLevel {
    
    if(_third) {
    
        [[Game shared] loadNextLevel];
    }
}

@end
