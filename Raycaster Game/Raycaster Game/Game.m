//
//  Game.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/25/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "Game.h"

@interface Game() {
    
    BOOL _loaded;
}

@property (retain, nonatomic) NSString * currentLevel, * previousLevel;

@end

@implementation Game

#pragma mark - Init

+ (id)shared {
    
    static Game * shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (id)init {
    
    if(self = [super init]) {
        
        _loaded = NO;
    }
    
    return self;
}

- (void)dealloc {
    
    [_levels release], _levels = nil;
    [super dealloc];
}

#pragma mark - Load

- (void)loadFirstLevel {
    
    [self loadDescription];
    [self loadLevel:_levels[_currentLevelIndex] previousLevel:@""];
}

- (void)loadDescription {
    
    if(!_levels) {
        
        NSDictionary * description = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:BUNDLE_FILE_PATH(@"Game")] options:0 error:nil];
        
        _levels = [description[@"levels"] retain];
        _currentLevelIndex = 0;
    }
}

- (void)loadNextLevel {
    
    unsigned nextLevel = (_currentLevelIndex + 1) % [_levels  count];
    
    [self.gameViewController clearImageCache];
    [self loadLevel:_levels[nextLevel] previousLevel:_levels[_currentLevelIndex]];

    _currentLevelIndex = nextLevel;
}

- (void)loadLevel:(NSString *)levelName previousLevel:(NSString *)previousLevelName {
    
    if(_loaded) {
        
        [self unloadLevel];
    }
    
    _currentLevel = [levelName retain];
    _previousLevel = [previousLevelName retain];
    
    _levelController = [[BasicLevelController loadLevel:levelName] retain];
    _player = [[Player alloc] init];
    
    _player.position = [_levelController spawnPosition];
    _player.angle = [_levelController spawnAngle];
    
    [_levelController levelDidLoad:previousLevelName];
    
    UIWindow * mainWindow = [[UIApplication sharedApplication] windows][0];
    
    [mainWindow setRootViewController:nil];
    
    _gameViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [mainWindow setRootViewController:_gameViewController];
    
    _loaded = YES;
}

- (void)reloadLevel {
    
    [self loadLevel:[NSString stringWithString:_currentLevel] previousLevel:[NSString stringWithString:_previousLevel]];
}

#pragma mark - Unload

- (void)unloadLevel {
    
    _loaded = NO;
    
    [_gameViewController unload];
    [_levelController levelWillUnload];
    
    _gameViewController = nil;
    [_player release], _player = nil;
    [_levelController release], _levelController = nil;
    [_currentLevel release], _currentLevel = nil;
    [_previousLevel release], _previousLevel = nil;
}

#pragma mark - Auxiliary methods

- (void)presentInfo:(NSString *)info withDuration:(NSTimeInterval)duration {
    
    [_gameViewController setInfoText:info];
    
    [_gameViewController performSelector:@selector(setInfoText:) withObject:@"" afterDelay:duration];
}

#pragma mark - Game Loop

- (void)gameLoopWithHeartbeat:(unsigned)heartbeat buttonFlags:(GameButtonFlags)buttonFlags interval:(float)interval {
    
    /* Variables from previous frame */
    int previousGridX = (int)floor(_player.position.v[0]/(float)GRID_LENGTH);
    int previousGridY = (int)floor(_player.position.v[1]/(float)GRID_LENGTH);
    unsigned previousFloorItem = GMItemAtPosition(_levelController.floor, previousGridX, previousGridY);
    
    /* Update Game State */
    [self updateTexturesWithHeartbeat:heartbeat];
    [self updatePathsWithHeartbeat:heartbeat];
    [_levelController levelUpdate:heartbeat];
    
    unsigned floorItem = GMItemAtPosition(_levelController.floor, previousGridX, previousGridY);
    
    /* Player Movement */
    BOOL moved = NO;
    
    Vector3 heading, right, newPosition = [[Game shared] player].position;
    
    if([_levelController levelAllowsPlayerMovement]) {
        
        if(buttonFlags & GameButtonFlagsLeft) {
            
            _player.angle -= MOVEMENT_FACTOR * interval;
        }
        
        if(buttonFlags & GameButtonFlagsRight) {
            
            _player.angle += MOVEMENT_FACTOR * interval;
        }
        
        _player.angle = IN360(_player.angle);
        
        if(XOR(buttonFlags & GameButtonFlagsUp,  buttonFlags & GameButtonFlagsDown)) {
            
            heading = scalarVector(MOVEMENT_FACTOR * interval, newVector(cosf(_player.angle * DEG2RAD), sinf(_player.angle * DEG2RAD), 0));
            
            if(buttonFlags & GameButtonFlagsUp) {
                
                newPosition = sumVector(_player.position, heading);
                moved = YES;
            }
            
            if(buttonFlags & GameButtonFlagsDown) {
                
                newPosition = subVector(_player.position, heading);
                moved = YES;
            }
        }
        
        if((buttonFlags & GameButtonFlagsStrafeLeft || buttonFlags & GameButtonFlagsStrafeRight)) {
            
            float rightAngle = _player.angle + 90.f; IN360(rightAngle);
            right = scalarVector(MOVEMENT_FACTOR * interval, newVector(cosf(rightAngle * DEG2RAD), sinf(rightAngle * DEG2RAD), 0));
            
            if(buttonFlags & GameButtonFlagsStrafeLeft) {
                
                newPosition = subVector(_player.position, right);
                moved = YES;
            }
            
            if(buttonFlags & GameButtonFlagsStrafeRight) {
                
                newPosition = sumVector(_player.position, right);
                moved = YES;
            }
        }
    }
    
    /* Collision detection */
    int gridPositionX = (int)floor(newPosition.v[0]/(float)GRID_LENGTH);
    int gridPositionY = (int)floor(newPosition.v[1]/(float)GRID_LENGTH);
    
    BOOL didUpdatePosition = NO;
    
    if(gridPositionX >= 0 && gridPositionX < _levelController.mapSize && gridPositionY >= 0 && gridPositionY < _levelController.mapSize) {
        
        unsigned item = GMItemAtPosition(_levelController.objects, gridPositionX, gridPositionY);
        
        if((item == 0 || ![[[[Game shared] levelController] objectAt:item] isImpassable]) && moved) {
            
            _player.position = newPosition;
            didUpdatePosition = YES;
        }
        
        if((didUpdatePosition && (previousGridX != gridPositionX || previousGridY != gridPositionY)) || previousFloorItem != floorItem) {
            
            [_levelController stepOverActionAt:GMItemAtPosition(_levelController.floor, gridPositionX, gridPositionY)];
            return;
        }
        
        if(item != 0) {
            
            [_levelController collisionActionAt:item];
            return;
        }
    }
}

- (void)updateTexturesWithHeartbeat:(unsigned)heartbeat {
    
    unsigned countOfObjects = [_levelController countOfObjects];
    
    for(unsigned i = 1; i <= countOfObjects; i++) {
        
        MapObject * mapObject = [_levelController objectAt:i];
        
        if([mapObject hasTexture]) {
            
            [[mapObject texture] updateWithHeartbeat:heartbeat];
        }
    }
    
    unsigned countOfCeilings = [_levelController countOfCeilings];
    
    for(unsigned i = 1; i <= countOfCeilings; i++) {
        
        MapCeiling * mapCeiling = [_levelController ceilingAt:i];
        
        if([mapCeiling hasTexture]) {
            
            [[mapCeiling texture] updateWithHeartbeat:heartbeat];
        }
    }
    
    unsigned countOfFloors = [_levelController countOfFloors];
    
    for(unsigned i = 1; i <= countOfFloors; i++) {
        
        MapFloor * mapFloor = [_levelController floorAt:i];
        
        if([mapFloor hasTexture]) {
            
            [[mapFloor texture] updateWithHeartbeat:heartbeat];
        }
    }
}

- (void)updatePathsWithHeartbeat:(unsigned)heartbeat {
    
    GameMap * objects = _levelController.objects;
    GameMap * ceiling = _levelController.ceiling;
    GameMap * floor = _levelController.floor;
    
    unsigned countOfObjects = [_levelController countOfObjects];
    
    for(unsigned i = 1; i <= countOfObjects; i++) {
        
        MapObject * mapObject = [_levelController objectAt:i];
        
        if([mapObject hasPath] && [[mapObject path] isActive]) {
            
            Path * path = [mapObject path];
            
            GMSetItemAtPosition(objects, path.currentX, path.currentY, [path replacement]);
            [[mapObject path] updateWithHeartbeat:heartbeat];
            GMSetItemAtPosition(objects, path.currentX, path.currentY, i);
        }
    }
    
    unsigned countOfCeilings = [_levelController countOfCeilings];
    
    for(unsigned i = 1; i <= countOfCeilings; i++) {
        
        MapCeiling * mapCeiling = [_levelController ceilingAt:i];
        
        if([mapCeiling hasPath] && [[mapCeiling path] isActive]) {
            
            Path * path = [mapCeiling path];
            
            GMSetItemAtPosition(ceiling, path.currentX, path.currentY, [path replacement]);
            [path updateWithHeartbeat:heartbeat];
            GMSetItemAtPosition(ceiling, path.currentX, path.currentY, i);
        }
    }
    
    unsigned countOfFloors = [_levelController countOfFloors];
    
    for(unsigned i = 1; i <= countOfFloors; i++) {
        
        MapFloor * mapFloor = [_levelController floorAt:i];
        
        if([mapFloor hasPath] && [[mapFloor path] isActive]) {
            
            Path * path = [mapFloor path];
            
            GMSetItemAtPosition(floor, path.currentX, path.currentY, [path replacement]);
            [[mapFloor path] updateWithHeartbeat:heartbeat];
            GMSetItemAtPosition(floor, path.currentX, path.currentY, i);
        }
    }
}

@end
