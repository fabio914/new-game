//
//  UIImage+ColorReplacement.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/16/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorReplacement)

- (UIImage *)imageByReplacingColors:(NSArray *)original withColors:(NSArray *)replacement;

@end
