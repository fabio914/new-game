//
//  Audio.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Audio : NSObject

@property (nonatomic) BOOL loops;
@property (nonatomic) BOOL stereo;

- (BOOL)hasAudio;
- (void)setAudio:(NSString *)audio;

- (unsigned)alBuffer;
- (unsigned)alSource;

@end
