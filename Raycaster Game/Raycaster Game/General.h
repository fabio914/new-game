//
//  General.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#ifndef Raycaster_Game_General_h
#define Raycaster_Game_General_h

#define HEARTBEATS_PER_SEC 60
#define GAME_INTERVAL 0.0167f
#define MOVEMENT_FACTOR 150.f

#define GRID_LENGTH 64.0f
#define WALL_HEIGHT 64.0f
#define GAME_FOV 60.0f /* degrees */
#define PLAYER_HEIGHT 32.0f

/* Angle algebra */
#define DEG2RAD (M_PI/180.0f)
#define RAD2DEG (180.0f/M_PI)
#define IN360(X) ((X) >= 360.0f) ? ((X) - 360.0f):(((X) < 0.0f) ? ((X) + 360.0f):(X))

typedef float Angle;

/* Logic */
#define XOR(X, Y) (((X) || (Y)) && !((X) && (Y)))

#ifdef __OBJC__

#define BUNDLE_FILE_PATH(X) \
[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:(X)]

#define alCheck(X) \
{ X; int alErr = alGetError(); if(alErr != 0) { DLOG(@"[%@]: Error calling " #X ":%d", NSStringFromClass([self class]), alErr); } }

#endif

/* Compilation Flags */
#define SKIP_INTRO

#endif
