//
//  UIImage+Rotation.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/11/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Rotation)

//- (UIImage *)imageByRotating:(CGFloat)angle;
- (UIImage *)imageByRotatingWithValue:(NSUInteger)value;

@end
