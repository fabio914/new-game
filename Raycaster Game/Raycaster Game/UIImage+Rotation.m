//
//  UIImage+Rotation.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/11/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "UIImage+Rotation.h"

@implementation UIImage (Rotation)

//- (UIImage *)imageByRotating:(CGFloat)angle {
//    
//    UIView * rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.size.width, self.size.height)];
//    
//    CGAffineTransform t = CGAffineTransformMakeRotation(angle);
//    rotatedViewBox.transform = t;
//    CGSize rotatedSize = rotatedViewBox.frame.size;
//    [rotatedViewBox release], rotatedViewBox = nil;
//    
//    UIGraphicsBeginImageContext(rotatedSize);
//    CGContextRef bitmap = UIGraphicsGetCurrentContext();
//    
//    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
//    
//    CGContextRotateCTM(bitmap, angle);
//    
//    CGContextScaleCTM(bitmap, 1.f, -1.f);
//    CGContextDrawImage(bitmap, CGRectMake(-self.size.width/2.f, -self.size.height/2.f, self.size.width, self.size.height), [self CGImage]);
//    
//    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return newImage;
//}

- (UIImage *)imageByRotatingWithValue:(NSUInteger)value {

    unsigned width = self.size.width;
    unsigned height = self.size.height;
    
    CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider([self CGImage]));
    const uint8_t * data = CFDataGetBytePtr(pixelData);
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    if(value == 0) {
    
        for(unsigned i = 0; i < width; i++) {
            
            for(unsigned j = 0; j < height; j++) {
                
                unsigned offset = ((width * j) + i) * 4;
                uint8_t r = data[offset], g = data[offset + 1], b = data[offset + 2], a =  data[offset + 3];
                
                UIColor * pixelColor = [[UIColor alloc] initWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f];
                
                CGContextSetFillColorWithColor(context, [pixelColor CGColor]);
                CGContextFillRect(context, CGRectMake(i, j, 1.f, 1.f));
                
                [pixelColor release];
            }
        }
    }
    
    else if(value == 1) { /* 90º */
        
        for(unsigned i = 0; i < width; i++) {
            
            for(unsigned j = 0; j < height; j++) {
                
                unsigned offset = ((width * j) + i) * 4;
                uint8_t r = data[offset], g = data[offset + 1], b = data[offset + 2], a =  data[offset + 3];
                
                UIColor * pixelColor = [[UIColor alloc] initWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f];
                
                CGContextSetFillColorWithColor(context, [pixelColor CGColor]);
                CGContextFillRect(context, CGRectMake(height - 1 - j, i, 1.f, 1.f));
                
                [pixelColor release];
            }
        }
    }
    
    else if(value == 2) { /* 180º */
        
        for(unsigned i = 0; i < width; i++) {
            
            for(unsigned j = 0; j < height; j++) {
                
                unsigned offset = ((width * j) + i) * 4;
                uint8_t r = data[offset], g = data[offset + 1], b = data[offset + 2], a =  data[offset + 3];
                
                UIColor * pixelColor = [[UIColor alloc] initWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f];
                
                CGContextSetFillColorWithColor(context, [pixelColor CGColor]);
                CGContextFillRect(context, CGRectMake(width - 1 - i, height - 1 - j, 1.f, 1.f));
                
                [pixelColor release];
            }
        }
    }
    
    else if(value == 3) { /* 270º */
        
        for(unsigned i = 0; i < width; i++) {
            
            for(unsigned j = 0; j < height; j++) {
                
                unsigned offset = ((width * j) + i) * 4;
                uint8_t r = data[offset], g = data[offset + 1], b = data[offset + 2], a =  data[offset + 3];
                
                UIColor * pixelColor = [[UIColor alloc] initWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f];
                
                CGContextSetFillColorWithColor(context, [pixelColor CGColor]);
                CGContextFillRect(context, CGRectMake(j, width - 1 - i, 1.f, 1.f));
                
                [pixelColor release];
            }
        }
    }
    
    CFRelease(pixelData);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
