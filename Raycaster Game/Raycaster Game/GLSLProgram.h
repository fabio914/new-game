//
//  GLSLProgram.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/8/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLSLProgram : NSObject

+ (GLSLProgram *)programWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment;

- (id)initWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment;

- (GLuint)glProgram;
- (void)use;
- (void)unload;

/* Remember to call "use" before using these methods */
- (GLint)getUniform:(NSString *)uniform;
- (GLint)getAttribute:(NSString *)attribute;
- (void)enableVertexAttribArrayFor:(NSString *)uniform;

@end
