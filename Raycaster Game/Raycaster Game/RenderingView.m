//
//  RenderingView.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/22/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "RenderingView.h"

@interface RenderingView()

@end

@implementation RenderingView

- (void)prepareForRendering {
    
}

- (void)updateHeartbeat:(unsigned)h {
    
    heartbeat = h;
}

- (void)renderFrame {
    
}

- (void)renderFrameWithHeartbeat:(unsigned)h {
    
    [self updateHeartbeat:h];
    [self renderFrame];
}

- (void)clearImageCache {
    
}

- (void)unload {
    
    _player = nil;
    _level = nil;
}

@end
