//
//  MapObject.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Texture.h"
#import "Audio.h"
#import "Path.h"

typedef enum {
    MapObjectTypeWall = 0,
    MapObjectTypeSprite
} MapObjectType;

@interface MapObject : NSObject

/* Static Object type */
@property (nonatomic) MapObjectType objectType;

@property (nonatomic, getter = isImpassable) BOOL impassable;

/* Textures */
@property (retain, nonatomic) AnimatedTexture * texture;

/* Audio */

/*
 * ATTENTION: You cannot have more than one copy of
 * an object if it has audio! (i.e. If object #3 has
 * audio, your map should have only one instance of #3
 * in its object matrix).
 */
@property (retain, nonatomic) Audio * audio;

/* Path */

/*
 * ATTENTION: You cannot have more than one copy of
 * an object if it has a path.
 */

@property (retain, nonatomic) Path * path;

/* Actions */
@property (retain, nonatomic) NSString * shootAction;
@property (retain, nonatomic) NSString * activateAction;
@property (retain, nonatomic) NSString * collisionAction;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (BOOL)hasTexture;
- (BOOL)hasAudio;
- (BOOL)hasPath;

- (BOOL)hasShootAction;
- (BOOL)hasActivateAction;
- (BOOL)hasCollisionAction;

@end
