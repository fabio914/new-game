//
//  Player.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "Player.h"

@implementation Player

+ (Player *)playerFromDictionary:(NSDictionary *)dictionary {
    
    Player * ret = [[Player alloc] init];
    
    ret.position = vectorFromDictionary(dictionary[@"position"]);
    ret.angle = [(NSNumber *)dictionary[@"angle"] doubleValue];
    
    return [ret autorelease];
}

- (NSDictionary *)dictionaryRepresentation {
    
    return @{@"position": dictionaryFromVector(self.position),
             @"angle": [NSNumber numberWithDouble:self.angle]};
}

@end
