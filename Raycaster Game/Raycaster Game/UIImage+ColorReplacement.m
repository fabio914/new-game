//
//  UIImage+ColorReplacement.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/16/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "UIImage+ColorReplacement.h"

@implementation UIImage (ColorReplacement)


#warning FIXME: THIS METHOD DOES NOT BEHAVE WELL ON 64-BIT IOS DEVICES!
/* Depois da rotacao (feita da maneira anterior) o vetor "data" nao fica com o numero correto de bytes e a largura e altura da imagem podem ficar errados por erro numerico */

- (UIImage *)imageByReplacingColors:(NSArray *)original withColors:(NSArray *)replacement {
    
    unsigned countOfColors = (unsigned)MIN([original count], [replacement count]);
    unsigned width = self.size.width;
    unsigned height = self.size.height;
    
    CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider([self CGImage]));
    const uint8_t * data = CFDataGetBytePtr(pixelData);
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for(unsigned i = 0; i < width; i++) {
        
        for(unsigned j = 0; j < height; j++) {
            
            unsigned offset = ((width * j) + i) * 4;
            uint8_t b = data[offset], g = data[offset + 1], r = data[offset + 2], a =  data[offset + 3];
            
            UIColor * pixelColor = [[UIColor alloc] initWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f];
            
            for(unsigned k = 0; k < countOfColors; k++) {
                
                CGFloat originalR, originalG, originalB, originalA;
                
#warning FIXME Pode dar problema no iOS 10
                [(UIColor *)[original objectAtIndex:k] getRed:&originalR green:&originalG blue:&originalB alpha:&originalA];
                
                uint8_t oR = (uint8_t)(originalR * 255.f),
                        oG = (uint8_t)(originalG * 255.f),
                        oB = (uint8_t)(originalB * 255.f),
                        oA = (uint8_t)(originalA * 255.f);
                
#warning FIXME Melhor nao comparar com o valor vindo do float (alternativa: utilizar estrutura de dados de imagens sempre com valores uint8)
                if(oR == r && oG == g && oB == b && oA == a) {
                    
                    [pixelColor release];
                    pixelColor = [[replacement objectAtIndex:k] retain];
                    break;
                }
            }
            
            CGContextSetFillColorWithColor(context, [pixelColor CGColor]);
            CGContextFillRect(context, CGRectMake(i, j, 1.f, 1.f));
            
            [pixelColor release];
        }
    }
    
    CFRelease(pixelData);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
