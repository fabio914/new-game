//
//  UIColor+Hex.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/16/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithRGB:(NSString *)rgb;
+ (UIColor *)colorWithRGBA:(NSString *)rgba;

@end
