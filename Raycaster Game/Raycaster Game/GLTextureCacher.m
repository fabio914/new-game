//
//  GLTextureCacher.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/26/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "GLTextureCacher.h"
#import <OpenGLES/ES2/gl.h>

#import "UIImage+Rotation.h"
#import "UIImage+ColorReplacement.h"
#import "UIColor+Hex.h"

@implementation GLTextureCacher

+ (id)shared {
    
    static GLTextureCacher * shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (UIImage *)processTexture:(NSString *)img {
    
    if(!_imageCache) {
        
        _imageCache = [[NSMutableDictionary alloc] init];
    }
    
    UIImage * image = [_imageCache objectForKey:img];
    
    if(image == nil) {
        
        NSArray * imageComponents = [img componentsSeparatedByString:@"."];
        
//        float rotation = 0.f;
        NSUInteger rotation = 0;
        
        /* Rotation */
        if([imageComponents count] > 1) {
            
            NSString * rotationString = [imageComponents objectAtIndex:1];
            
//            if([rotationString isEqualToString:@"1"]) {
//                
//                rotation = M_PI/2.f;
//            }
//            
//            else if([rotationString isEqualToString:@"2"]) {
//                
//                rotation = M_PI;
//            }
//            
//            else if([rotationString isEqualToString:@"3"]) {
//                
//                rotation = 3.f * M_PI/2.f;
//            }
            
            if([rotationString isEqualToString:@"1"])
                rotation = 1;
            
            else if([rotationString isEqualToString:@"2"])
                rotation = 2;
            
            else if([rotationString isEqualToString:@"3"])
                rotation = 3;
        }
        
//        image = [[[UIImage imageNamed:[imageComponents objectAtIndex:0]] imageByRotating:rotation] retain];
        image = [[[UIImage imageNamed:[imageComponents objectAtIndex:0]] imageByRotatingWithValue:rotation] retain];
        
        /* Color replacement */
        if([imageComponents count] > 2 && [imageComponents count] <= 5) {
            
            NSMutableArray * replacements = [NSMutableArray array];
            NSArray * originals = @[[UIColor colorWithRGB:@"FF0000"],
                                    [UIColor colorWithRGB:@"00FF00"],
                                    [UIColor colorWithRGB:@"0000FF"]];
            
            for(unsigned k = 2; k < [imageComponents count]; k++) {
                
                if([[imageComponents objectAtIndex:k] length] == 0) {
                    
                    [replacements addObject:[originals objectAtIndex:(k - 2)]];
                }
                
                else {
                    
                    [replacements addObject:[UIColor colorWithRGB:[imageComponents objectAtIndex:k]]];
                }
            }
            
            UIImage * newImage = [image imageByReplacingColors:originals withColors:replacements];
            [image release];
            image = [newImage retain];
        }
        
        [_imageCache setObject:image forKey:img];
        [image release];
    }
    
    return image;
}

- (GLuint)loadTexture:(NSString *)img {
    
    if(!_loadedTextures) {
        
        _loadedTextures = [[NSMutableDictionary alloc] init];
    }
    
    NSNumber * textureId = [_loadedTextures objectForKey:img];
    
    if(textureId == nil) {
        
        textureId = [NSNumber numberWithUnsignedInt:[self setupTexture:img]];
        [_loadedTextures setObject:textureId forKey:img];
    }
    
    return (GLuint)[textureId unsignedIntValue];
}

- (GLuint)setupTexture:(NSString *)img {
    
    CGImageRef spriteImage = [[self processTexture:img] CGImage];
    
    size_t width = CGImageGetWidth(spriteImage);
    size_t height = CGImageGetHeight(spriteImage);
    
    GLubyte * spriteData ;
    
    if((spriteData = (GLubyte *)calloc(width * height * 4, sizeof(GLubyte))) == NULL) {
        
        return 0;
    }
    
    CGContextRef spriteContext = CGBitmapContextCreate(spriteData, width, height, 8, width * 4, CGImageGetColorSpace(spriteImage), kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextTranslateCTM(spriteContext, 0, height);
    CGContextScaleCTM(spriteContext, 1.0, -1.0);
    
    CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), spriteImage);
    
    CGContextRelease(spriteContext);
    
    GLuint texName;
    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    
#ifdef USE_MIPMAP
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
#else
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
#endif
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
    
#ifdef USE_MIPMAP
    glHint(GL_GENERATE_MIPMAP_HINT, GL_FASTEST);
    glGenerateMipmap(GL_TEXTURE_2D);
#endif
    
    free(spriteData);
    
    return texName;
}

- (void)unloadCache {
    
    [_imageCache release], _imageCache = nil;
}

- (void)unloadTextures {
    
    [_loadedTextures release], _loadedTextures = nil;
}

@end
