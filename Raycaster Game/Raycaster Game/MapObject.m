//
//  MapObject.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "MapObject.h"
#include "General.h"

@implementation MapObject

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if(self = [super init]) {
        
        _impassable = [(NSNumber *)dictionary[@"impassable"] boolValue];
        
        /* Must be a 64x64 image! */
        if(dictionary[@"texture"]) {
            
            _texture = [[AnimatedTexture animatedTextureFromDescription:dictionary[@"texture"]] retain];
        }
        
        if(dictionary[@"audio"]) {
            
            _audio = [[Audio alloc] init];
            _audio.audio = dictionary[@"audio"][@"sound"];
            _audio.loops = [dictionary[@"audio"][@"loops"] boolValue];
        }
        
        if(dictionary[@"path"]) {
            
            _path = [[Path pathWithDescription:dictionary[@"path"]] retain];
        }
        
        _shootAction = [(NSString *)dictionary[@"shootAction"] retain];
        _activateAction = [(NSString *)dictionary[@"activateAction"] retain];
        _collisionAction = [(NSString *)dictionary[@"collideAction"] retain];
        
        if(dictionary[@"type"] && [dictionary[@"type"] isKindOfClass:[NSString class]]) {
            
            NSString * typeString = dictionary[@"type"];
            
            if([typeString isEqualToString:@"wall"]) {
                
                _objectType = MapObjectTypeWall;
            }
            
            else if([typeString isEqualToString:@"sprite"]) {
                
                _objectType = MapObjectTypeSprite;
            }
        }
    }
    
    return self;
}

- (void)dealloc {
    
    [_texture release], _texture = nil;
    [_audio release], _audio = nil;
    [_path release], _path = nil;
    
    [_shootAction release], _shootAction = nil;
    [_activateAction release], _activateAction = nil;
    [_collisionAction release], _collisionAction = nil;
    
    [super dealloc];
}

- (BOOL)hasTexture {
    
    return _texture != nil && [_texture hasImage];
}

- (BOOL)hasAudio {
    
    return _audio != nil && [_audio hasAudio];
}

- (BOOL)hasPath {
    
    return _path != nil && [_path hasPath];
}

- (BOOL)hasShootAction {
    
    return  _shootAction != nil;
}

- (BOOL)hasActivateAction {
    
    return _activateAction != nil;
}

- (BOOL)hasCollisionAction {
    
    return _collisionAction != nil;
}

@end
