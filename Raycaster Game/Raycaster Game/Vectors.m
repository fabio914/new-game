//
//  Vectors.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/19/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#include "Vectors.h"

NSDictionary * dictionaryFromVector(Vector3 v) {

    return @{@"x": [NSNumber numberWithDouble:v.v[0]],
             @"y": [NSNumber numberWithDouble:v.v[1]],
             @"z": [NSNumber numberWithDouble:v.v[2]]};
}

Vector3 vectorFromDictionary(NSDictionary * dictionary) {
    
    return newVector([(NSNumber *)dictionary[@"x"] doubleValue],
                     [(NSNumber *)dictionary[@"y"] doubleValue],
                     [(NSNumber *)dictionary[@"z"] doubleValue]);
}
