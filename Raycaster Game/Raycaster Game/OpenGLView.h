//
//  OpenGLView.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/23/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "RenderingView.h"

@interface OpenGLView : RenderingView

@end
