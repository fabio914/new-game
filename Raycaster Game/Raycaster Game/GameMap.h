//
//  GameMap.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#ifndef Raycaster_Game_GameMap_h
#define Raycaster_Game_GameMap_h

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    uint8_t ** matrix;
    unsigned mapSize;
} GameMap;

GameMap * GMCreateMap(unsigned mapSize);
uint8_t GMItemAtPosition(GameMap * map, int gridX, int gridY);
void GMSetItemAtPosition(GameMap * map, int gridX, int gridY, uint8_t value);
void GMReleaseMap(GameMap ** map);

#ifdef __OBJC__
NSDictionary * GMDictionaryRepresentation(GameMap * map);
GameMap * GMCreateWithDictionary(NSDictionary * dictionary);
#endif

#endif
