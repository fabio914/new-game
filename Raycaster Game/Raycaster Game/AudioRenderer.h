//
//  AudioRenderer.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "General.h"
#include "Vectors.h"

#import "Player.h"
#import "BasicLevelController.h"

@interface AudioRenderer : NSObject

@property (assign, nonatomic) Player * player;
@property (assign, nonatomic) BasicLevelController * level;

/* Override */
- (void)prepareForRendering;
- (void)renderAudio;
- (void)unload;

@end
