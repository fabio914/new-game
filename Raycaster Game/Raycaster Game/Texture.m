//
//  Texture.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "Texture.h"
#import "GLTextureCacher.h"

#include "General.h"

@interface Texture()

@property (retain, nonatomic) NSString * image;

@property (nonatomic) GLuint textureId;

@end

@implementation Texture

- (void)setImage:(NSString *)image {
    
    NSArray * imageComponents = [image componentsSeparatedByString:@"."];
    
    UIImage * uiImage = [UIImage imageNamed:[imageComponents objectAtIndex:0]];
    
    if(uiImage.size.width == GRID_LENGTH && uiImage.size.height == WALL_HEIGHT) {
        
        _image = [image retain];
        _alpha = [self alphaImage:uiImage];
    }
    
    else {
        
        _image = nil;
    }
}

- (void)dealloc {
    
    [_image release], _image = nil;
    
    [super dealloc];
}

- (BOOL)alphaImage:(UIImage *)img {
    
    CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider([img CGImage]));
    const uint8_t * data = CFDataGetBytePtr(pixelData);
    
    for(unsigned i = 0; i < img.size.width; i++) {
        
        for(unsigned j = 0; j < img.size.height; j++) {
            
            unsigned offset = (((int)(img.size.width)  * j) + i) * 4 + 3;
            
            if(data[offset] != 255) {
                
                CFRelease(pixelData);
                return YES;
            }
        }
    }
    
    CFRelease(pixelData);
    return NO;
}

- (BOOL)hasImage {
    
    return _image != nil;
}

- (GLuint)glTexture {
    
    if(_textureId == 0) {
        
        _textureId = [[GLTextureCacher shared] loadTexture:_image];
    }
    
    return _textureId;
}

@end

@interface AnimatedTexture()

@property (nonatomic) unsigned currentTexture;

@end

@implementation AnimatedTexture

+ (id)animatedTextureFromDescription:(NSObject *)description {
    
    AnimatedTexture * texture = nil;
    
    if([description isKindOfClass:[NSString class]]) {
        
        texture = [[AnimatedTexture alloc] initWithImages:@[description] mode:AnimatedTextureModeStill interval:1];
    }
    
    else if([description isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary * dictionary = (NSDictionary *)description;
        
        AnimatedTextureMode mode = AnimatedTextureModeStill;
        unsigned interval = 1;
        
        if(dictionary[@"animation"]) {
            
            NSString * modeString = (NSString *)dictionary[@"animation"][@"mode"];
            
            if([modeString isEqualToString:@"ordered"]) {
                
                mode = AnimatedTextureModeOrdered;
            }
            
            else if([modeString isEqualToString:@"random"]) {
                
                mode = AnimatedTextureModeRandom;
            }
            
            interval = [(NSNumber *)dictionary[@"animation"][@"interval"] unsignedIntValue];
        }
        
        texture = [[AnimatedTexture alloc] initWithImages:dictionary[@"images"] mode:mode interval:interval];
    }
    
    return [texture autorelease];
}

- (id)initWithImages:(NSArray *)images mode:(AnimatedTextureMode)mode interval:(unsigned)interval {
    
    if(self = [super init]) {
        
        NSMutableArray * textures = [NSMutableArray array];
        
        for(NSString * image in images) {
            
            Texture * tex = [[Texture alloc] init];
            [tex setImage:image];
            
            [textures addObject:tex];
            [tex release];
        }
        
        _textures = [textures retain];
        _mode = mode;
        _interval = interval;
        _animates = (mode != AnimatedTextureModeStill);
        _currentTexture = 0;
    }
    
    return self;
}

- (void)dealloc {
    
    [_textures release], _textures = nil;
    
    [super dealloc];
}

- (BOOL)hasImage {
    
    return [_textures count] > 0 && [(Texture *)[_textures objectAtIndex:_currentTexture] hasImage];
}

- (BOOL)hasAlpha {
    
    return [(Texture *)[_textures objectAtIndex:_currentTexture] hasAlpha];
}

- (GLuint)glTexture {
    
    return [(Texture *)[_textures objectAtIndex:_currentTexture] glTexture];
}

- (void)setCurrentTexture:(unsigned)currentTexture {
    
    if(_mode == AnimatedTextureModeStill && currentTexture < [_textures count]) {
        
        _currentTexture = currentTexture;
    }
}

- (void)updateWithHeartbeat:(unsigned)heartbeat {
    
    if(_animates && (heartbeat % _interval) == 0) {
        
        if(_mode == AnimatedTextureModeOrdered) {
            
            _currentTexture = (_currentTexture + 1) % [_textures count];
        }
        
        else if(_mode == AnimatedTextureModeRandom) {
            
            _currentTexture = rand() % [_textures count];
        }
    }
}


@end
