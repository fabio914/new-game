//
//  GLTextureCacher.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/26/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLTextureCacher : NSObject

@property (retain, nonatomic) NSMutableDictionary * imageCache;
@property (retain, nonatomic) NSMutableDictionary * loadedTextures;

+ (id)shared;
- (GLuint)loadTexture:(NSString *)img;
- (void)unloadCache;
- (void)unloadTextures;

@end

