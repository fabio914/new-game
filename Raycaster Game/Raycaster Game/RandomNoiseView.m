//
//  RandomNoiseView.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/19/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "RandomNoiseView.h"

#include <time.h>

@implementation RandomNoiseView

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
    
        srand(time(NULL));
    }
    return self;
}

- (void)randomize {
    
    [self setNeedsDisplay];
}

CGImageRef CGCreateNoiseImage(CGSize size, CGFloat factor) {
    
    NSUInteger bits = fabs(size.width) * fabs(size.height);
    char *rgba = (char *)malloc(bits);
    
    for(int i = 0; i < bits; ++i)
        rgba[i] = (rand() % 256) * factor;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapContext = CGBitmapContextCreate(rgba, fabs(size.width), fabs(size.height),
                                                       8, fabs(size.width), colorSpace, 0);
    CGImageRef image = CGBitmapContextCreateImage(bitmapContext);
    
    CFRelease(bitmapContext);
    CGColorSpaceRelease(colorSpace);
    free(rgba);
    
    return image;
}

- (void)drawRect:(CGRect)rect {

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetShouldAntialias(context, NO);
    CGContextSetAllowsAntialiasing(context, NO);
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    
    CGSize size = CGSizeMake(rect.size.width/4.0, rect.size.height/4.0);
    
    CGImageRef cgImage = CGCreateNoiseImage(size, 50.0);
    CGContextDrawImage(context, rect, cgImage);
    CGImageRelease(cgImage);
}


@end
