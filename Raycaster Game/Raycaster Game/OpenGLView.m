//
//  OpenGLView.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/23/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "OpenGLView.h"
#import "GLTextureCacher.h"
#import "GLSLProgram.h"

#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include "glMath.h"

@interface OpenGLView() {
    
    CAEAGLLayer * _eaglLayer;
    EAGLContext * _context;
    
    GLuint _frameBuffer;
    GLuint _colorRenderBuffer;
    GLuint _depthRenderBuffer;
    GLuint _vertexBuffer;
    GLuint _indexBuffer;
    
    glMathContext_t * mathContext;
    
    /* Auxiliary */
    Vector3 _right, _leftFOV, _rightFOV, _playerPosition;
    
    GLSLProgram * _currentProgram;
    
    double scale;
}

@property (retain, nonatomic) GLSLProgram * normalProgram;
@property (retain, nonatomic) GLSLProgram * alphaProgram;

@end

@implementation OpenGLView

typedef struct {
    float Position[3];
    float Color[4];
    float TexCoord[2];
} Vertex;

#define TEX_COORD_MAX 1

const Vertex Vertices[] = {
    // Front
    {{1, -1, 1}, {1, 1, 1, 1}, {TEX_COORD_MAX, 0}},
    {{1, 1, 1}, {1, 1, 1, 1}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{-1, 1, 1}, {1, 1, 1, 1}, {0, TEX_COORD_MAX}},
    {{-1, -1, 1}, {1, 1, 1, 1}, {0, 0}},
    // Back
    {{1, 1, -1}, {1, 1, 1, 1}, {TEX_COORD_MAX, 0}},
    {{1, -1, -1}, {1, 1, 1, 1}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{-1, -1, -1}, {1, 1, 1, 1}, {0, TEX_COORD_MAX}},
    {{-1, 1, -1}, {1, 1, 1, 1}, {0, 0}},
    // Left
    {{-1, -1, 1}, {1, 1, 1, 1}, {TEX_COORD_MAX, 0}},
    {{-1, 1, 1}, {1, 1, 1, 1}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{-1, 1, -1}, {1, 1, 1, 1}, {0, TEX_COORD_MAX}},
    {{-1, -1, -1}, {1, 1, 1, 1}, {0, 0}},
    // Right
    {{1, -1, -1}, {1, 1, 1, 1}, {TEX_COORD_MAX, 0}},
    {{1, 1, -1}, {1, 1, 1, 1}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{1, 1, 1}, {1, 1, 1, 1}, {0, TEX_COORD_MAX}},
    {{1, -1, 1}, {1, 1, 1, 1}, {0, 0}},
    // Top
    {{1, 1, 1}, {1, 1, 1, 1}, {TEX_COORD_MAX, 0}},
    {{1, 1, -1}, {1, 1, 1, 1}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{-1, 1, -1}, {1, 1, 1, 1}, {0, TEX_COORD_MAX}},
    {{-1, 1, 1}, {1, 1, 1, 1}, {0, 0}},
    // Bottom
    {{1, -1, -1}, {1, 1, 1, 1}, {TEX_COORD_MAX, 0}},
    {{1, -1, 1}, {1, 1, 1, 1}, {TEX_COORD_MAX, TEX_COORD_MAX}},
    {{-1, -1, 1}, {1, 1, 1, 1}, {0, TEX_COORD_MAX}},
    {{-1, -1, -1}, {1, 1, 1, 1}, {0, 0}}
};

const GLubyte Indices[] = {
    // Front
    0, 1, 2,
    2, 3, 0,
    // Back
    4, 5, 6,
    6, 7, 4,
    // Left
    8, 9, 10,
    10, 11, 8,
    // Right
    12, 13, 14,
    14, 15, 12,
    // Top
    16, 17, 18,
    18, 19, 16,
    // Bottom
    20, 21, 22,
    22, 23, 20
};

#pragma mark - View

- (void)dealloc {

    [super dealloc];
}

+ (Class)layerClass {
    
    return [CAEAGLLayer class];
}

#pragma mark - OpenGL methods

- (void)setupLayer {
    
//#ifdef RETINA_SCALE
    if([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES) {
        
        scale = [[UIScreen mainScreen] scale];
        self.contentScaleFactor = scale;
    }
    
    else {
        
        scale = 1.0;
    }
//#else
//    scale = 1.0;
//#endif
    
    _eaglLayer = (CAEAGLLayer *)self.layer;
    _eaglLayer.opaque = YES;
    _eaglLayer.drawableProperties = @{kEAGLDrawablePropertyRetainedBacking: @YES,
                                      kEAGLDrawablePropertyColorFormat: kEAGLColorFormatRGB565};
}

- (void)setupContext {
    
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    
    if (!_context) {
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
        exit(1);
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to set current OpenGL context");
        exit(1);
    }
}

- (void)setupRenderBuffer {
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
}

- (void)setupDepthBuffer {
    glGenRenderbuffers(1, &_depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, scale * self.frame.size.width, scale * self.frame.size.height);
}

- (void)setupFrameBuffer {
    
    glGenFramebuffers(1, &_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthRenderBuffer);
}

- (void)compileShaders {
    
    self.normalProgram = [GLSLProgram programWithVertexShader:@"SimpleVertex" fragmentShader:@"SimpleFragment"];
    [_normalProgram use];
    [_normalProgram enableVertexAttribArrayFor:@"Position"];
    [_normalProgram enableVertexAttribArrayFor:@"SourceColor"];
    [_normalProgram enableVertexAttribArrayFor:@"TexCoordIn"];
    
    self.alphaProgram = [GLSLProgram programWithVertexShader:@"SimpleVertex" fragmentShader:@"AlphaFragment"];
    [_alphaProgram use];
    [_alphaProgram enableVertexAttribArrayFor:@"Position"];
    [_alphaProgram enableVertexAttribArrayFor:@"SourceColor"];
    [_alphaProgram enableVertexAttribArrayFor:@"TexCoordIn"];
}

- (void)setupVBOs {
    
    glGenBuffers(1, &_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
    
    glGenBuffers(1, &_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
}

#pragma mark - "Frustum Culling"

- (BOOL)point:(Vector3)pt isRightOfVector:(Vector3)vt initialPoint:(Vector3)ipt {
    
    if(vt.v[0] >= 0) {
        
        if(vt.v[1] >= 0) { /* (+, +) */
            
            float xl = ipt.v[0] + vt.v[0] * ((pt.v[1] - ipt.v[1])/vt.v[1]);
            
            return (pt.v[0] <= xl);
        }
        
        else { /* (+, -) */
            
            float yl = ipt.v[1] + vt.v[1] * ((pt.v[0] - ipt.v[0])/vt.v[0]);
            
            return (pt.v[1] >= yl);
        }
    }
    
    else {
        
        if(vt.v[1] >= 0) { /* (-, +) */
            
            float yl = ipt.v[1] + vt.v[1] * ((pt.v[0] - ipt.v[0])/vt.v[0]);
            
            return (pt.v[1] <= yl);
        }
        
        else { /* (-, -) */
            
            float xl = ipt.v[0] + vt.v[0] * ((pt.v[0] - ipt.v[1])/vt.v[1]);
            
            return (pt.v[0] >= xl);
        }
    }
    
    return NO;
}

- (BOOL)isVisiblePoint:(Vector3)pt {
    
/*
 
 3       2
 \      /
  \FOV /
   \  /
    \/____1
 
 Posicao do grid deve entrar no pipeline, se ao menos 1 dos 4 vertices
 estiverem: à esquerda de (1), à esquerda de (2) e à direita de (3).
 
*/
    
    return (![self point:pt isRightOfVector:_right initialPoint:_playerPosition] &&
            ![self point:pt isRightOfVector:_rightFOV initialPoint:_playerPosition] &&
             [self point:pt isRightOfVector:_leftFOV initialPoint:_playerPosition]);
}

- (BOOL)isVisibleGridPositionX:(unsigned)x Y:(unsigned)y {
    
//    Vector3 pos = newVector(x * GRID_LENGTH, y * GRID_LENGTH, 0);
//    
//    if([self isVisiblePoint:pos]) {
//        
//        return YES;
//    }
//    
//    pos.v[0] += GRID_LENGTH;
//    
//    if([self isVisiblePoint:pos]) {
//        
//        return YES;
//    }
//    
//    pos.v[1] += GRID_LENGTH;
//    
//    if([self isVisiblePoint:pos]) {
//        
//        return YES;
//    }
//    
//    pos.v[0] -= GRID_LENGTH;
//    
//    if([self isVisiblePoint:pos]) {
//        
//        return YES;
//    }
//    
//    return NO;
    
#warning FIXME
    
    return YES;
}

- (void)updateCullingVectors {
    
    _playerPosition = self.player.position;
    
    float rightAngle = self.player.angle + 90.f; IN360(rightAngle);
    _right = newVector(cosf(rightAngle * DEG2RAD), sinf(rightAngle * DEG2RAD), 0);
    
    float rightFOVAngle = self.player.angle + GAME_FOV/2.f; IN360(rightFOVAngle);
    _rightFOV = newVector(cosf(rightFOVAngle * DEG2RAD), sinf(rightFOVAngle * DEG2RAD), 0);
    
    float leftFOVAngle = self.player.angle - GAME_FOV/2.f; IN360(rightFOVAngle);
    _leftFOV = newVector(cosf(leftFOVAngle * DEG2RAD), sinf(leftFOVAngle * DEG2RAD), 0);
}

- (void)printCulling {
    
    int mapSize = self.level.mapSize;
    
    for(int y = 0; y < mapSize; y++) {
        
        for(int x = 0; x < mapSize; x++) {
            
            printf("%d ", (int)[self isVisibleGridPositionX:x Y:y]);
        }
        
        printf("\n");
    }
    
    printf("\n");
}

#pragma mark - Auxiliary methods

- (void)setBlockAttribPointers {

    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);
    
    glVertexAttribPointer([_currentProgram getAttribute:@"Position"], 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer([_currentProgram getAttribute:@"SourceColor"], 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 3));
    
    glVertexAttribPointer([_currentProgram getAttribute:@"TexCoordIn"], 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 7));
}

- (void)drawSkybox {
    
    if([self.level.skybox hasImage]) {

        glDisable(GL_DEPTH_TEST);

        mglMatrixMode(mathContext, GL_MODELVIEW);
        mglPushMatrix(mathContext);
        mglLoadIdentity(mathContext);
        
        mgluLookAt(mathContext,
                   cosf(self.player.angle * DEG2RAD), 0, sinf(self.player.angle * DEG2RAD),
                   0, 0, 0,
                   0, 1, 0);
        mglScalef(mathContext, 10.f, 10.f, 10.f);
        
        glUniformMatrix4fv([_currentProgram getUniform:@"Modelview"], 1, 0, glMathGetMatrix(mathContext, GL_MODELVIEW));
        mglPopMatrix(mathContext);
        
        glActiveTexture(GL_TEXTURE0);
        
        glBindTexture(GL_TEXTURE_2D, [self.level.skybox glTexture]);
        glUniform1i([_currentProgram getUniform:@"Texture"], 0);
        
        glDrawElements(GL_TRIANGLES, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
        
        glEnable(GL_DEPTH_TEST);
    }
}

#pragma mark - RenderingView methods

- (void)prepareForRendering {
    
    [self setupLayer];
    [self setupContext];
    [self setupDepthBuffer];
    [self setupRenderBuffer];
    [self setupFrameBuffer];
    [self compileShaders];
    [self setupVBOs];
    
    mathContext = glMathInit(glMathAlloc());
    
    glEnable(GL_DEPTH_TEST);
//    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
//    glEnable(GL_BLEND);
    
#warning TODO
//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_BACK);
    
    /* Retina display */
    glViewport(0, 0, scale * self.frame.size.width, scale * self.frame.size.height);
    
    mglMatrixMode(mathContext, GL_PROJECTION);
    mglLoadIdentity(mathContext);
    mgluPerspective(mathContext, GAME_FOV, self.frame.size.width/self.frame.size.height, .1f, 9999.0f);
}

- (void)drawWithAlpha:(BOOL)alpha {
    
    int mapSize = self.level.mapSize;
    
    for(int ceiling = 0; ceiling < [self.level countOfCeilings]; ceiling++) {
        
        AnimatedTexture * tex = [[self.level ceilingAt:ceiling + 1] texture];
        
        if([tex hasImage]) {
            
            glBindTexture(GL_TEXTURE_2D, ([tex hasImage]) ? [tex glTexture]:0);
            glUniform1i([_currentProgram getUniform:@"Texture"], 0);
        }
        
        if([tex hasAlpha] == alpha) {
        
            for(register int x = 0; x < mapSize; x++) {
                
                for(register int y = 0; y < mapSize; y++) {
                    
                    if(GMItemAtPosition(self.level.ceiling, x, y) == (ceiling + 1) && [self isVisibleGridPositionX:x Y:y]) {
                        
                        mglPushMatrix(mathContext);
                        mglTranslatef(mathContext, x * GRID_LENGTH + 0.5 * GRID_LENGTH, WALL_HEIGHT/2, y * GRID_LENGTH + 0.5 * GRID_LENGTH);
                        mglScalef(mathContext, GRID_LENGTH/2, WALL_HEIGHT/2, GRID_LENGTH/2);
                        
                        glUniformMatrix4fv([_currentProgram getUniform:@"Modelview"], 1, 0, glMathGetMatrix(mathContext, GL_MODELVIEW));
                        
                        mglPopMatrix(mathContext);
                        
                        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, (GLvoid *)24);
                    }
                }
            }
        }
    }
    
    for(int mp = 0; mp < [self.level countOfObjects]; mp++) {
        
        BOOL isSprite = [[self.level objectAt:mp + 1] objectType] == MapObjectTypeSprite;
        
        AnimatedTexture * tex = [[self.level objectAt:mp + 1] texture];
        
        if([tex hasImage]) {
            
            glBindTexture(GL_TEXTURE_2D, ([tex hasImage]) ? [tex glTexture]:0);
            glUniform1i([_currentProgram getUniform:@"Texture"], 0);
        }
        
        if([tex hasAlpha] == alpha) {
            
            for(register int x = 0; x < mapSize; x++) {
                
                for(register int y = 0; y < mapSize; y++) {
                    
                    if(GMItemAtPosition(self.level.objects, x, y) == (mp + 1) && [self isVisibleGridPositionX:x Y:y]) {
                        
                        if(isSprite) {
                            
                            mglPushMatrix(mathContext);
                            
                            Vector3 position = newVector(self.player.position.v[0], PLAYER_HEIGHT, self.player.position.v[1]);
                            Vector3 direction = normalizeVector(subVector(position, newVector(x * GRID_LENGTH + 0.5 * GRID_LENGTH, PLAYER_HEIGHT, y * GRID_LENGTH + 0.5 * GRID_LENGTH)));
                            Vector3 right = crossVector(newVector(0, -1, 0), direction);
                            Vector3 up = crossVector(direction, right);
                            right = crossVector(up, direction);
                            
                            mglTranslatef(mathContext, x * GRID_LENGTH + 0.5 * GRID_LENGTH, WALL_HEIGHT/2, y * GRID_LENGTH + 0.5 * GRID_LENGTH);
                            
#warning TODO Create a function that generates this matrix
                            
                            GLfloat m[16] = {right.v[0],  up.v[0], direction.v[0], 0.f,
                                right.v[1], -up.v[1], direction.v[1], 0.f,
                                right.v[2],  up.v[2], direction.v[2], 0.f,
                                0.f,      0.f,            0.f, 1.f};
                            
                            mglMultMatrixf(mathContext, m);
                            
                            mglScalef(mathContext, -GRID_LENGTH/2, WALL_HEIGHT/2, 1);
                            mglTranslatef(mathContext, 0, 0, -1);
                            
                            glUniformMatrix4fv([_currentProgram getUniform:@"Modelview"], 1, 0, glMathGetMatrix(mathContext, GL_MODELVIEW));
                            
                            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
                            
                            mglPopMatrix(mathContext);
                        }
                        
                        else {
                            
                            mglPushMatrix(mathContext);
                            mglTranslatef(mathContext, x * GRID_LENGTH + 0.5 * GRID_LENGTH, WALL_HEIGHT/2, y * GRID_LENGTH + 0.5 * GRID_LENGTH);
                            mglScalef(mathContext, GRID_LENGTH/2, WALL_HEIGHT/2, GRID_LENGTH/2);
                            
                            glUniformMatrix4fv([_currentProgram getUniform:@"Modelview"], 1, 0, glMathGetMatrix(mathContext, GL_MODELVIEW));
                            
                            mglPopMatrix(mathContext);
                            
                            glDrawElements(GL_TRIANGLES, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
                        }
                    }
                }
            }
        }
    }
    
    for(int floor = 0; floor < [self.level countOfFloors]; floor++) {
        
        AnimatedTexture * tex = [[self.level floorAt:floor + 1] texture];
        
        if([tex hasImage]) {
            
            glBindTexture(GL_TEXTURE_2D, ([tex hasImage]) ? [tex glTexture]:0);
            glUniform1i([_currentProgram getUniform:@"Texture"], 0);
        }
        
        if([tex hasAlpha] == alpha) {
            
            for(register int x = 0; x < mapSize; x++) {
                
                for(register int y = 0; y < mapSize; y++) {
                    
                    if(GMItemAtPosition(self.level.floor, x, y) == (floor + 1) && [self isVisibleGridPositionX:x Y:y]) {
                        
                        mglPushMatrix(mathContext);
                        mglTranslatef(mathContext, x * GRID_LENGTH + 0.5 * GRID_LENGTH, WALL_HEIGHT/2, y * GRID_LENGTH + 0.5 * GRID_LENGTH);
                        mglScalef(mathContext, GRID_LENGTH/2, WALL_HEIGHT/2, GRID_LENGTH/2);
                        
                        glUniformMatrix4fv([_currentProgram getUniform:@"Modelview"], 1, 0, glMathGetMatrix(mathContext, GL_MODELVIEW));
                        
                        mglPopMatrix(mathContext);
                        
                        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, (GLvoid *)30);
                    }
                }
            }
        }
    }
}

- (void)renderFrame {
    
    /* Use normal program */
    [_normalProgram use]; _currentProgram = _normalProgram;
    glUniform2f([_currentProgram getUniform:@"rainbowFactor"], 1.f/(scale * self.frame.size.width), heartbeat/(float)HEARTBEATS_PER_SEC);
    glUniformMatrix4fv([_currentProgram getUniform:@"Projection"], 1, 0, glMathGetMatrix(mathContext, GL_PROJECTION));
    [self setBlockAttribPointers];
    
    [self updateCullingVectors];
    
    glClearColor(0.4f, 0.7f, 0.8f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glActiveTexture(GL_TEXTURE0);
    
    [self drawSkybox];
    
    mglMatrixMode(mathContext, GL_MODELVIEW);
    
    mglLoadIdentity(mathContext);
    mgluLookAtDir(mathContext,
               self.player.position.v[0], PLAYER_HEIGHT, self.player.position.v[1],
               cosf(self.player.angle * DEG2RAD), 0.f, sinf(self.player.angle * DEG2RAD),
               0.0f, 1.0f, 0.0f);
    
    [self drawWithAlpha:NO];
    
    /* Use alpha program */
    [_alphaProgram use]; _currentProgram = _alphaProgram;
    glUniform2f([_currentProgram getUniform:@"rainbowFactor"], 1.f/(scale * self.frame.size.width), heartbeat/(float)HEARTBEATS_PER_SEC);
    glUniformMatrix4fv([_currentProgram getUniform:@"Projection"], 1, 0, glMathGetMatrix(mathContext, GL_PROJECTION));
    [self setBlockAttribPointers];
    
    /* Then alpha images */
    
    [self drawWithAlpha:YES];
    
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)clearImageCache {
    
    [[GLTextureCacher shared] unloadCache];
}

- (void)unload {
    
    [[GLTextureCacher shared] unloadTextures];
    
    glMathRelease(mathContext);
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteBuffers(1, &_indexBuffer);
    
    [self.normalProgram unload];
    [_normalProgram release], _normalProgram = nil;
    
    [self.alphaProgram unload];
    [_alphaProgram release], _alphaProgram = nil;
    
    glDeleteRenderbuffers(1, &_colorRenderBuffer);
    glDeleteRenderbuffers(1, &_depthRenderBuffer);
    glDeleteFramebuffers(1, &_frameBuffer);
    
    [EAGLContext setCurrentContext:nil];
    [_context release], _context = nil;
    
    [super unload];
}

@end
