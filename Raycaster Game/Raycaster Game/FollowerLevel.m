//
//  FollowerLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/26/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "FollowerLevel.h"
#import "Game.h"

@interface FollowerLevel()

@property (nonatomic) BOOL didStartMoving;
@property (nonatomic) BOOL switchState;

@property (retain, nonatomic) NSMutableArray * blockPosition;
@property (retain, nonatomic) NSArray * blockValidArea;

@end

@implementation FollowerLevel

- (void)levelDidLoad:(NSString *)previousLevel {
    
    _didStartMoving = NO;
    
    _switchState = [self.extraData[@"switchInitialState"] boolValue];
    [[[self objectAt:4] texture] setCurrentTexture:_switchState ? 0:1];
    
    _blockPosition = [[NSMutableArray arrayWithArray:self.extraData[@"blockInitialPosition"]] retain];
    _blockValidArea = [self.extraData[@"blockValidArea"] retain];
    
    GMSetItemAtPosition(self.objects, [_blockPosition[0] intValue], [_blockPosition[1] intValue], 3);
}

- (void)levelUpdate:(unsigned int)heartbeat {
    
    /* Every second */
    if(heartbeat % (HEARTBEATS_PER_SEC) == 0) {
        
        int blockX = [_blockPosition[0] intValue], blockY = [_blockPosition[1] intValue];
        
        GMSetItemAtPosition(self.objects, blockX, blockY, 0);
        
        if(_switchState && _didStartMoving) {
            
            Player * p = [[Game shared] player];
            
            int diffX = (int)(p.position.v[0]/GRID_LENGTH) - blockX;
            int diffY = (int)(p.position.v[1]/GRID_LENGTH) - blockY;
            
            int incrementX = diffX/fabs(diffX), incrementY = diffY/fabs(diffY);
            
            blockX += incrementX; blockY += incrementY;
            
            int minX = [_blockValidArea[0][0] intValue], maxX = [_blockValidArea[1][0] intValue];
            int minY = [_blockValidArea[0][1] intValue], maxY = [_blockValidArea[1][1] intValue];
            
            _blockPosition[0] = (blockX >= minX && blockX <= maxX) ? @(blockX):_blockPosition[0];
            _blockPosition[1] = (blockY >= minY && blockY <= maxY) ? @(blockY):_blockPosition[1];
        }
        
        GMSetItemAtPosition(self.objects, [_blockPosition[0] intValue], [_blockPosition[1] intValue], 3);
    }
}

- (GameAction)startMoving {
    
    if(!_didStartMoving) {
        
        [[Game shared] presentInfo:@"It's following you..." withDuration:2.0];
        _didStartMoving = YES;
    }
}

- (GameAction)switchChange {
    
    _switchState = !_switchState;
    
    [[[self objectAt:4] texture] setCurrentTexture:_switchState ? 0:1];
}

- (GameAction)killPlayer {
    
    [[Game shared] reloadLevel];
}

- (GameAction)nextLevel {
    
    if(_switchState) {
        
        [[Game shared] presentInfo:@"You better turn that thing off before you go..." withDuration:2.0];
    }
    
    else {
        
        [[Game shared] loadNextLevel];
    }
}

- (void)levelWillUnload {
    
    [_blockPosition release], _blockPosition = nil;
    [_blockValidArea release], _blockValidArea = nil;
    
    [super levelWillUnload];
}

@end
