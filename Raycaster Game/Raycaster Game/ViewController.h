//
//  ViewController.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/19/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "General.h"

#import "RenderingView.h"
#import "AudioRenderer.h"

typedef enum {
    GameButtonFlagsNone    = 0,
    GameButtonFlagsLeft    = 1 << 0,
    GameButtonFlagsRight   = 1 << 1,
    GameButtonFlagsUp      = 1 << 2,
    GameButtonFlagsDown    = 1 << 3,
    GameButtonFlagsA       = 1 << 4,
    GameButtonFlagsB       = 1 << 5,
    GameButtonFlagsStrafeLeft = 1 << 6,
    GameButtonFlagsStrafeRight = 1 << 7
} GameButtonFlags;

@interface ViewController : UIViewController

@property (assign, nonatomic) IBOutlet RenderingView * gameView;
@property (retain, nonatomic) IBOutlet AudioRenderer * audioRenderer;
@property (assign, nonatomic) IBOutlet UILabel * infoLabel;


- (void)prepareForGame;
- (void)prepareForRendering;
- (void)clearImageCache;
- (void)unload;

- (void)setInfoText:(NSString *)text;

@end

@interface LeftView : UIView {
    
    @protected
    GameButtonFlags _buttonFlags;
    BOOL isMoving;
}

@property (retain, nonatomic) UIImageView * joyPad;
@property (retain, nonatomic) UIImageView * joyButton;

- (GameButtonFlags)buttonFlags;

@end

@interface RightView : LeftView
@end
