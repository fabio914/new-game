//
//  BasicLevelController.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MapCeiling.h"
#import "MapObject.h"
#import "MapFloor.h"
#import "Texture.h"
#import "Audio.h"

#include "General.h"
#include "GameMap.h"
#include "Vectors.h"

/* Similar to IBAction */
#define GameAction void

@interface BasicLevelController : NSObject

@property (retain, nonatomic) Texture * skybox;
@property (retain, nonatomic) Audio * globalAudio;
@property (retain, nonatomic) NSDictionary * extraData;

/* This is it! */
+ (BasicLevelController *)loadLevel:(NSString *)level;

- (id)initWithDictionary:(NSDictionary *)levelDictionary;

- (unsigned)mapSize;

- (GameMap *)ceiling;
- (GameMap *)objects;
- (GameMap *)floor;

- (Vector3)spawnPosition;
- (Angle)spawnAngle;
- (double)maxDistance;

- (MapCeiling *)ceilingAt:(unsigned)index;
- (unsigned)countOfCeilings;

- (MapObject *)objectAt:(unsigned)index;
- (unsigned)countOfObjects;

- (MapFloor *)floorAt:(unsigned)index;
- (unsigned)countOfFloors;

- (void)shootActionAt:(unsigned)index;
- (void)activateActionAt:(unsigned)index;
- (void)collisionActionAt:(unsigned)index;
- (void)stepOverActionAt:(unsigned)index;

- (void)replaceObjectOfKind:(unsigned)o withKind:(unsigned)n;

/* Overrride those (remember to call "super") */

/* One of these is called once for level */
- (void)levelDidLoad:(NSString *)previousLevel;
- (void)levelDidLoadFromState:(NSDictionary *)state;

/* Called for every heartbeat */
- (void)levelUpdate:(unsigned)heartbeat;

/* Called once for level */
- (void)levelWillUnload;

/* Saving... */
- (BOOL)levelAllowsSaving;
- (NSDictionary *)levelSaveState;

/* Player movement */
- (BOOL)levelAllowsPlayerMovement;

@end
