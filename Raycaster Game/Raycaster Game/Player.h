//
//  Player.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "General.h"
#include "Vectors.h"

@interface Player : NSObject

@property (nonatomic) Vector3 position; /* in World Coordinates */
@property (nonatomic) Angle angle; /* in degrees */

+ (Player *)playerFromDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryRepresentation;

@end
