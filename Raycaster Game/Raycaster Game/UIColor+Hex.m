//
//  UIColor+Hex.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/16/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "UIColor+Hex.h"

uint8_t hexComponentFromString(NSString * hex) {
    
    if([hex length] != 2) {
        
        return 0;
    }
    
    NSString * str = [hex uppercaseString];
    unichar high = [str characterAtIndex:0];
    unichar low = [str characterAtIndex:1];
    uint8_t ret = 0;
    
    if(high >= 'A' && high <= 'F') {
        ret += (high - 'A' + 10) << 4;
    }
    
    else if(high >= '0' && high <= '9') {
        ret += (high - '0') << 4;
    }
    
    if(low >= 'A' && low <= 'F') {
        ret += (low - 'A' + 10);
    }
    
    else if(low >= '0' && low <= '9') {
        ret += (low - '0');
    }
    
    return ret;
}

@implementation UIColor (Hex)

+ (UIColor *)colorWithRGB:(NSString *)rgb {
    
    NSString * str = [rgb stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if([str length] != 6) {
        
        return [UIColor blackColor];
    }
    
    return [self colorWithRGBA:[str stringByAppendingString:@"FF"]];
}

+ (UIColor *)colorWithRGBA:(NSString *)rgba {
    
    NSString * str = [rgba stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if([str length] != 8) {
        
        return [UIColor blackColor];
    }
    
    CGFloat r = hexComponentFromString([str substringWithRange:(NSRange){0, 2}])/255.f;
    CGFloat g = hexComponentFromString([str substringWithRange:(NSRange){2, 2}])/255.f;
    CGFloat b = hexComponentFromString([str substringWithRange:(NSRange){4, 2}])/255.f;
    CGFloat a = hexComponentFromString([str substringWithRange:(NSRange){6, 2}])/255.f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

@end

