//
//  ALAudioCacher.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "ALAudioCacher.h"

#import <AudioToolbox/AudioToolbox.h>
#import <OpenAL/al.h>

#include "General.h"

#define OPEN_AL_MAX_BUFFERS 32

@implementation ALAudioCacher

+ (id)shared {
    
    static ALAudioCacher * shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

- (unsigned)loadSound:(NSString *)sound stereo:(BOOL)stereo {
    
    if(!_loadedSounds) {
        
        _loadedSounds = [[NSMutableDictionary alloc] init];
    }
    
    NSNumber * soundId = [_loadedSounds objectForKey:sound];
    
    if(soundId == nil) {
        
        soundId = [NSNumber numberWithUnsignedInt:[self setupSound:sound stereo:stereo]];
        [_loadedSounds setObject:soundId forKey:sound];
        
        if([_loadedSounds count] >= OPEN_AL_MAX_BUFFERS) {
            
            DLOG(@"Warning: OpenAL maximum number of buffers reached!");
        }
    }
    
    return [soundId unsignedIntValue];
}

- (UInt32)audioFileSize:(AudioFileID)fileDescriptor {
    
	UInt64 outDataSize = 0;
	UInt32 thePropSize = sizeof(UInt64);
    OSStatus result = 1;
    
	if((result = AudioFileGetProperty(fileDescriptor, kAudioFilePropertyAudioDataByteCount, &thePropSize, &outDataSize)) != 0) {
        
        return 0;
    }
	
    return (UInt32)outDataSize;
}

- (unsigned)setupSound:(NSString *)sound stereo:(BOOL)stereo {
    
    /* afconvert -f caff -d LEI16@44100 in.mp3 out.caf */
    
    NSString * path = [[NSBundle mainBundle] pathForResource:sound ofType:@"caf"];
    
    AudioFileID outAFID = 0;
    NSURL * afURL = [NSURL fileURLWithPath:path];
    
    if(AudioFileOpenURL((__bridge CFURLRef)afURL, kAudioFileReadPermission, 0, &outAFID) != 0) {
        
        return 0;
    }

    UInt32 fileSize = [self audioFileSize:outAFID];
    uint8_t * outData;
    
    if(fileSize == 0 || (outData = (uint8_t *)malloc(fileSize * sizeof(uint8_t))) == NULL) {
        
        AudioFileClose(outAFID);
        return 0;
    }
    
    if((AudioFileReadBytes(outAFID, NO, 0, &fileSize, outData)) != 0) {
        
        free(outData);
        AudioFileClose(outAFID);
        return 0;
    }
    
    unsigned bufferId = 0;
    
    alCheck(alGenBuffers(1, &bufferId));
    alCheck(alBufferData(bufferId, stereo ? AL_FORMAT_STEREO16:AL_FORMAT_MONO16, outData, fileSize, 44100));
    
    free(outData);
    AudioFileClose(outAFID);
    
    return bufferId;
}

- (void)unloadSounds {
    
    [_loadedSounds release], _loadedSounds = nil;
}

@end
