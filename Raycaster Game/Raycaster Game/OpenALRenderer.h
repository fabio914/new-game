//
//  OpenALRenderer.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "AudioRenderer.h"

@interface OpenALRenderer : AudioRenderer

@end
