//
//  InitialViewController.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "InitialViewController.h"
#import "RandomNoiseView.h"

#import "Game.h"
#include "General.h"

@import AVFoundation;

@interface InitialViewController ()

@property (assign, nonatomic) IBOutlet RandomNoiseView * noiseView;
@property (assign, nonatomic) IBOutlet UIView * mainMenuView;

@property (retain, nonatomic) AVAudioPlayer * player;

@end

@implementation InitialViewController

- (BOOL)prefersStatusBarHidden {
    
    return YES;
}

- (void)dealloc {
    
    [_player release], _player = nil;
    
    [super dealloc];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.noiseView.alpha = 0.0;
    
    self.mainMenuView.userInteractionEnabled = NO;
    self.mainMenuView.alpha = 0.0;
    
    NSError * err = nil;
    
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:[[[NSURL alloc] initFileURLWithPath:BUNDLE_FILE_PATH(@"Noise.mp3")] autorelease] error:&err];
    
    [self.player prepareToPlay];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self updateNoiseView];
    [self.player play];
    
#ifndef SKIP_INTRO
    
    [UIView animateWithDuration:2.0 animations:^{
        
        self.noiseView.alpha = 1.0;
        
    } completion:^(BOOL finished) {
       
        [self performSelector:@selector(showMenu) withObject:nil afterDelay:4.0];
    }];
    
#else
    
    self.noiseView.alpha = 1.0;
    [self showMenu];
    
#endif
}

- (void)showMenu {
    
#ifndef SKIP_INTRO
    
    [UIView animateWithDuration:2.0 animations:^{
        
        self.mainMenuView.alpha = 0.7;
        
    } completion:^(BOOL finished) {
        
        self.mainMenuView.userInteractionEnabled = YES;
    }];
    
#else
    
    self.mainMenuView.alpha = 0.7;
    self.mainMenuView.userInteractionEnabled = YES;
    
#endif
}

- (void)updateNoiseView {
    
    [self.noiseView randomize];
    
    [self performSelector:@selector(updateNoiseView) withObject:nil afterDelay:0.033];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (IBAction)playButtonAction:(id)sender {

    [[Game shared] loadFirstLevel];
}

@end
