//
//  Audio.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "Audio.h"
#import "ALAudioCacher.h"

#import <AudioToolbox/AudioToolbox.h>
#import <OpenAL/al.h>

#include "General.h"

@interface Audio()

@property (retain, nonatomic) NSString * audio;
@property (nonatomic) unsigned bufferId, sourceId;

@end

@implementation Audio

- (void)setAudio:(NSString *)audio {
    
    [_audio release], _audio = nil;
    _audio = [audio retain];
}

- (void)dealloc {
    
    [_audio release], _audio = nil;
    [super dealloc];
}

- (BOOL)hasAudio {
    
    return _audio != nil;
}

- (unsigned)alBuffer {
    
    if(_bufferId == 0) {
        
        _bufferId = [[ALAudioCacher shared] loadSound:_audio stereo:_stereo];
    }
    
    return _bufferId;
}

- (unsigned)alSourceWithLoop:(BOOL)loops {
    
    unsigned sourceId;
    
    alCheck(alGenSources(1, &sourceId));
    alCheck(alSourcei(sourceId, AL_BUFFER, [self alBuffer]));
    
    alCheck(alSourcef(sourceId, AL_PITCH, 1.f));
    alCheck(alSourcef(sourceId, AL_GAIN, 1.f));
    
    if(loops) {
        
        alCheck(alSourcei(sourceId, AL_LOOPING, AL_TRUE));
    }
    
    alCheck(alSourcePlay(sourceId));
    
    return sourceId;
}

/*
 * You must not reuse this source for more than one
 * map object in your map's object matrix.
 *
 * That is, if you have an object with audio, there
 * should be only one of this object in that matrix.
 */
- (unsigned)alSource {
    
    if(_sourceId == 0) {
        
        _sourceId = [self alSourceWithLoop:_loops];
    }
    
    return _sourceId;
}

@end
