//
//  ViewController.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/19/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "ViewController.h"

#import "Game.h"

#include "Vectors.h"

@interface ViewController () {
    
    GameButtonFlags _buttonFlags;
    unsigned heartbeat;
    
    BOOL isMovingJoypad, isMovingSecondJoypad;
}

@property (retain, nonatomic) LeftView * leftControl;
@property (retain, nonatomic) RightView * rightControl;

@property (retain, nonatomic) UIImageView * gun;

@property (retain, nonatomic) CADisplayLink * displayLink;

@end

@implementation ViewController

- (BOOL)prefersStatusBarHidden {
    
    return YES;
}

#pragma mark - View Events

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.alpha = 0.f;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self prepareForGame];
    [self prepareForRendering];
    
    [self.gameView setPlayer:[[Game shared] player]];
    [self.gameView setLevel:[[Game shared] levelController]];
    
    [self.audioRenderer setPlayer:[[Game shared] player]];
    [self.audioRenderer setLevel:[[Game shared] levelController]];
    
//    _gun = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.gameView.frame.size.width/3.f, self.gameView.frame.size.width/3.f)];
//    [_gun setImage:[UIImage imageNamed:@"gun1"]];
//    _gun.center = CGPointMake(0.75f * self.gameView.frame.size.width, self.gameView.frame.size.height - _gun.frame.size.height/2.f);
//    [self.view addSubview:_gun];
    
    /* Left Stick */
    _leftControl = [[LeftView alloc] initWithFrame:CGRectMake(0, 0, self.gameView.frame.size.width/2.f, self.gameView.frame.size.height)];
    _leftControl.exclusiveTouch = NO;
    _leftControl.multipleTouchEnabled = NO;
    _leftControl.joyButton = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"joybutton"]] autorelease];
    _leftControl.joyPad = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"joypad"]] autorelease];
    [self.view addSubview:_leftControl];
    
    /* Right Stick */
    _rightControl = [[RightView alloc] initWithFrame:CGRectMake(self.gameView.frame.size.width/2.f, 0, self.gameView.frame.size.width/2.f, self.gameView.frame.size.height)];
    _rightControl.exclusiveTouch = NO;
    _rightControl.multipleTouchEnabled = NO;
    _rightControl.joyButton = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"joybutton"]] autorelease];
    _rightControl.joyPad = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"joypad"]] autorelease];
    [self.view addSubview:_rightControl];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.alpha = 1.f;
    } completion:^(BOOL finished) {
        
        if(!finished) {
            self.view.alpha = 1.f;
        }
    }];
}

- (void)dealloc {
    
    [_leftControl release], _leftControl = nil;
    [_rightControl release], _rightControl = nil;
    [_gun release], _gun = nil;
    
    [super dealloc];
}

#pragma mark - Main Loop

- (void)prepareForGame {
    
    [self.infoLabel setText:@""];
    
    _buttonFlags = GameButtonFlagsNone;
    
    heartbeat = 0;
    
    _displayLink = nil;
    _displayLink = [[CADisplayLink displayLinkWithTarget:self selector:@selector(gameLoop:)] retain];
    _displayLink.frameInterval = 1; /* ~ 60 fps */
    
    [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)gameLoop:(CADisplayLink *)link {
    
    @autoreleasepool {
        
        /* Rendering */
        [self.gameView renderFrameWithHeartbeat:heartbeat];
        [self.audioRenderer renderAudio];
        
        /* Heartbeat */
        heartbeat = (heartbeat + 1) % HEARTBEATS_PER_SEC;
        
        /* Update game */
        [[Game shared] gameLoopWithHeartbeat:heartbeat buttonFlags:[self.leftControl buttonFlags] | [self.rightControl buttonFlags] interval:[link duration]];
        
        /* Preparing next frame */
        [self clearABFlags];
    }
}

#pragma mark - Rendering

- (void)prepareForRendering {
    
    [self.gameView setUserInteractionEnabled:NO];
    
    /* Prepare Game View */
    [self.gameView prepareForRendering];
    
    /* Prepare Audio */
    [self.audioRenderer prepareForRendering];
}

#pragma mark - Unload

- (void)clearImageCache {
    
    [self.gameView clearImageCache];
}

- (void)unload {
    
    [_displayLink setPaused:YES];
    [_displayLink invalidate];
    [_displayLink release], _displayLink = nil;
    
    [_gameView unload];
    _gameView = nil;
    
    [_audioRenderer unload];
    [_audioRenderer release], _audioRenderer = nil;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark - Game Provider

- (void)setInfoText:(NSString *)text {
    
    [self.infoLabel setText:text];
}

#pragma mark - Joypad

- (IBAction)AButtonAction:(id)sender {

    _buttonFlags |= GameButtonFlagsA;
}

- (IBAction)BButtonAction:(id)sender {

    _buttonFlags |= GameButtonFlagsB;
}

- (void)clearABFlags {
    
    _buttonFlags &= ~(GameButtonFlagsA | GameButtonFlagsB);
}

- (void)printActiveButtons {
    
    DLOG(@"left:%d right:%d up:%d down:%d A:%d B:%d s_left:%d s_right:%d",
         (_buttonFlags & GameButtonFlagsLeft),
         (_buttonFlags & GameButtonFlagsRight) >> 1,
         (_buttonFlags & GameButtonFlagsUp)    >> 2,
         (_buttonFlags & GameButtonFlagsDown)  >> 3,
         (_buttonFlags & GameButtonFlagsA)     >> 4,
         (_buttonFlags & GameButtonFlagsB)     >> 5,
         (_buttonFlags & GameButtonFlagsStrafeLeft) >> 6,
         (_buttonFlags & GameButtonFlagsStrafeRight) >> 6);
}

#pragma mark - Others

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

@end

#pragma mark - Virtual Dual Stick Views

@implementation LeftView

- (void)dealloc {
    
    [_joyPad release], _joyPad = nil;
    [_joyButton release], _joyButton = nil;
    
    [super dealloc];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch * touch = [touches anyObject];
    CGPoint touchPosition = [touch locationInView:self];
    
    if(!isMoving) {
        
        _joyPad.center = touchPosition;
        _joyButton.center = touchPosition;
        [self addSubview:_joyPad];
        [self addSubview:_joyButton];
        
        isMoving = YES;
    }
}

- (void)clearFlags {
    
    _buttonFlags &= ~GameButtonFlagsUp;
    _buttonFlags &= ~GameButtonFlagsDown;
    _buttonFlags &= ~GameButtonFlagsStrafeLeft;
    _buttonFlags &= ~GameButtonFlagsStrafeRight;
}

- (void)stopTouch {
    
    if(isMoving) {
        
        [_joyPad removeFromSuperview];
        [_joyButton removeFromSuperview];
        [self clearFlags];
        isMoving = NO;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self stopTouch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self stopTouch];
}

- (void)updateFlagsWithTouchPoint:(CGPoint)touchPoint centre:(CGPoint)joyPadCentre radiusThreshold:(float)radiusThreshold {
    
    float deltaX = touchPoint.x - joyPadCentre.x;
    float deltaY = touchPoint.y - joyPadCentre.y;
    BOOL leftRight = (fabs(deltaX) > radiusThreshold);
    BOOL upDown = (fabs(deltaY) > radiusThreshold);
    
    if(deltaX > 0 && leftRight) {
        
        _buttonFlags |= GameButtonFlagsStrafeRight;
    }
    
    if(deltaX < 0 && leftRight) {
        
        _buttonFlags |= GameButtonFlagsStrafeLeft;
    }
    
    if(deltaY > 0 && upDown) {
        
        _buttonFlags |= GameButtonFlagsDown;
    }
    
    if(deltaY < 0 && upDown) {
        
        _buttonFlags |= GameButtonFlagsUp;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch * touch = [touches anyObject];
    
    if(isMoving) {
        
        [self clearFlags];
        
        CGPoint touchPoint = [touch locationInView:self];
        CGPoint joyPadCentre = self.joyPad.center;
        float joyPadRadius = self.joyPad.frame.size.width/2.0f;
        float radiusThreshold = self.joyPad.frame.size.width/4.0f;
        
        float currentRadius = sqrt((touchPoint.x - joyPadCentre.x)*(touchPoint.x - joyPadCentre.x) + (touchPoint.y - joyPadCentre.y)*(touchPoint.y - joyPadCentre.y));
        
        if(currentRadius < joyPadRadius) {
            
            self.joyButton.center = touchPoint;
        }
        
        else {
            
            float angle = atan2f((touchPoint.y - joyPadCentre.y), (touchPoint.x - joyPadCentre.x));
            CGPoint newPoint = CGPointMake(joyPadCentre.x + cosf(angle) * joyPadRadius, joyPadCentre.y + sinf(angle) * joyPadRadius);
            
            currentRadius = joyPadRadius;
            touchPoint = newPoint;
            
            self.joyButton.center = newPoint;
        }
        
        if(currentRadius >= radiusThreshold) {
            
            [self updateFlagsWithTouchPoint:touchPoint centre:joyPadCentre radiusThreshold:radiusThreshold];
        }
    }
}

- (GameButtonFlags)buttonFlags {
    
    return _buttonFlags;
}

@end

@implementation RightView

- (void)clearFlags {
    
    _buttonFlags &= ~GameButtonFlagsUp;
    _buttonFlags &= ~GameButtonFlagsDown;
    _buttonFlags &= ~GameButtonFlagsRight;
    _buttonFlags &= ~GameButtonFlagsLeft;
}

- (void)updateFlagsWithTouchPoint:(CGPoint)touchPoint centre:(CGPoint)joyPadCentre radiusThreshold:(float)radiusThreshold {
    
    float deltaX = touchPoint.x - joyPadCentre.x;
    float deltaY = touchPoint.y - joyPadCentre.y;
    BOOL leftRight = (fabs(deltaX) > radiusThreshold);
    BOOL upDown = (fabs(deltaY) > radiusThreshold);
    
    if(deltaX > 0 && leftRight) {
        
        _buttonFlags |= GameButtonFlagsRight;
    }
    
    if(deltaX < 0 && leftRight) {
        
        _buttonFlags |= GameButtonFlagsLeft;
    }
    
    if(deltaY > 0 && upDown) {
        
        _buttonFlags |= GameButtonFlagsDown;
    }
    
    if(deltaY < 0 && upDown) {
        
        _buttonFlags |= GameButtonFlagsUp;
    }
}

@end
