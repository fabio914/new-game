//
//  Game.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/25/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "General.h"

#import "Player.h"
#import "BasicLevelController.h"

#import "ViewController.h"

@interface Game : NSObject

@property (retain, nonatomic) NSArray * levels;
@property (nonatomic, readonly) unsigned currentLevelIndex;

@property (retain, nonatomic) Player * player;
@property (retain, nonatomic) BasicLevelController * levelController;

@property (assign, nonatomic) ViewController * gameViewController;

+ (id)shared;

- (void)loadFirstLevel;
- (void)loadNextLevel;
- (void)reloadLevel;
- (void)unloadLevel;

- (void)presentInfo:(NSString *)info withDuration:(NSTimeInterval)duration;

- (void)gameLoopWithHeartbeat:(unsigned)heartbeat buttonFlags:(GameButtonFlags)buttonFlags interval:(float)interval;
- (void)updateTexturesWithHeartbeat:(unsigned)heartbeat;
- (void)updatePathsWithHeartbeat:(unsigned)heartbeat;

@end
