//
//  Texture.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Texture : NSObject

@property (nonatomic, readonly, getter = hasAlpha) BOOL alpha;

- (BOOL)hasImage;
- (GLuint)glTexture;
- (void)setImage:(NSString *)image;

@end

typedef enum {
    AnimatedTextureModeStill = 0,
    AnimatedTextureModeOrdered,
    AnimatedTextureModeRandom
} AnimatedTextureMode;

@interface AnimatedTexture : NSObject

@property (retain, nonatomic, readonly) NSArray * textures; // of Texture

@property (nonatomic, readonly) BOOL animates;
@property (nonatomic, readonly) AnimatedTextureMode mode;
@property (nonatomic, readonly) unsigned interval;

+ (id)animatedTextureFromDescription:(NSObject *)description;

- (id)initWithImages:(NSArray *)images mode:(AnimatedTextureMode)mode interval:(unsigned)interval;
- (BOOL)hasAlpha;
- (BOOL)hasImage;
- (GLuint)glTexture;

- (void)setCurrentTexture:(unsigned)currentTexture;
- (void)updateWithHeartbeat:(unsigned)heartbeat;

@end
