#ifdef GL_ES
precision lowp float;
#endif

varying lowp vec4 DestinationColor;

varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;

uniform lowp vec2 rainbowFactor;

vec4 h2rgb(float c) {
    
    const highp vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(vec3(c) + K.xyz) * 6.0 - K.www);
    return vec4(clamp(p - K.xxx, 0.0, 1.0), 1.0);
}

void main(void) {
    
    lowp vec4 color = /*DestinationColor **/ texture2D(Texture, TexCoordOut);
    
    /* Alpha testing */
    if(color.a == 0.0)
        discard;
    
    /* Rainbow effect */
    const vec3 replacementColor = vec3(1.0, 0.0, 1.0);
    
    if(color.rgb == replacementColor) {
        
        color = h2rgb((gl_FragCoord.x) * rainbowFactor.x + rainbowFactor.y);
    }
    
    /* Darken distant fragments */
//    float z = 1.0 - (gl_FragCoord.z/gl_FragCoord.w)/2000.0;
//    z = clamp(z, 0.0, 1.0);
//    color = vec4(z * color.xyz, 1.0);
    
    gl_FragColor = color;
}
