//
//  Path.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/29/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PathModeLoop = 0,
    PathModeChanging,
    PathModeOnce
} PathMode;

@interface Path : NSObject

@property (nonatomic, readonly) unsigned interval;
@property (nonatomic, readonly) unsigned replacement;
@property (nonatomic, readonly) PathMode mode;
@property (nonatomic, getter = isActive) BOOL active;

@property (retain, nonatomic, readonly) NSArray * coordinates;

+ (Path *)pathWithDescription:(NSDictionary *)dictionary;
- (id)initWithCoordiantes:(NSArray *)coordinates mode:(PathMode)mode interval:(unsigned)interval active:(BOOL)active replacement:(unsigned)replacement;

- (BOOL)hasPath;

- (void)updateWithHeartbeat:(unsigned)heartbeat;

- (unsigned)currentX;
- (unsigned)currentY;

@end
