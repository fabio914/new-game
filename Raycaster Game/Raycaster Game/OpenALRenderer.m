//
//  OpenALRenderer.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "OpenALRenderer.h"
#import "ALAudioCacher.h"

#import <AudioToolbox/AudioToolbox.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>

#include "General.h"

@interface OpenALRenderer() {
    
    ALCcontext * context;
    ALCdevice * device;
}

@end

@implementation OpenALRenderer

- (void)prepareForRendering {
    
    if((device = alcOpenDevice(NULL)) != 0) {
        
        context = alcCreateContext(device, NULL);
        alcMakeContextCurrent(context);
    }
}

- (void)renderAudio {
    
    GameMap * objectsMap = self.level.objects;
    unsigned mapSize = self.level.mapSize;
    
    /* Objects */
    for(int x = 0; x < mapSize; x++) {
        
        for(int y = 0; y < mapSize; y++) {
            
            MapObject * object = [self.level objectAt:GMItemAtPosition(objectsMap, x, y)];
            
            if([object hasAudio]) {
                
                unsigned source = [[object audio] alSource];
                
                float position[3] = {x * GRID_LENGTH + 0.5 * GRID_LENGTH, PLAYER_HEIGHT, y * GRID_LENGTH + 0.5 * GRID_LENGTH};
                float velocity[3] = {0, 0, 0};
                
                alCheck(alSourcefv(source, AL_POSITION, position));
                alCheck(alSourcefv(source, AL_VELOCITY, velocity));
            }
        }
    }
    
    /* Listener */
    float listenerPosition[3] = {self.player.position.v[0], PLAYER_HEIGHT, self.player.position.v[1]};
    
#warning TODO Trocar por (direcao de movimento) * (tamanho do passo) * (tempo entre frames)
    float listenerVelocity[3] = {0, 0, 0};
    
    float listenerOrientation[6] = {cosf(self.player.angle * DEG2RAD), 0.f, sinf(self.player.angle * DEG2RAD), 0.f, 1.f, 0.f};
    
    alCheck(alListenerfv(AL_POSITION, listenerPosition));
    alCheck(alListenerfv(AL_VELOCITY, listenerVelocity));
    alCheck(alListenerfv(AL_ORIENTATION, listenerOrientation));
    
    /* Global audio */
    if([self.level.globalAudio hasAudio]) {
        
        unsigned source = [self.level.globalAudio alSource];
        
        alCheck(alSourcefv(source, AL_POSITION, listenerPosition));
        alCheck(alSourcefv(source, AL_VELOCITY, listenerVelocity));
    }
}

- (void)unload {
    
    alCheck(alcDestroyContext(context));
    alCheck(alcCloseDevice(device));
    
    [[ALAudioCacher shared] unloadSounds];
    
    [super unload];
}

@end
