//
//  BasicLevelController.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "BasicLevelController.h"

@interface BasicLevelController() {
    
    unsigned _mapSize;
    
    GameMap * ceilMap;
    GameMap * objectMap;
    GameMap * floorMap;
    
    unsigned spawnGridX, spawnGridY;
    double _spawnAngle;
}

@property (retain, nonatomic) NSArray * ceilingKinds; /* of MapCeiling */
@property (retain, nonatomic) NSArray * objectKinds; /* of MapObject */
@property (retain, nonatomic) NSArray * floorKinds; /* of MapFloor */

@end

@implementation BasicLevelController

#pragma mark - Init

+ (BasicLevelController *)loadLevel:(NSString *)level {

    /* Does not check if level data exists, or if it's a valid JSON! */
    
    Class levelClass = NSClassFromString(level);

    if(levelClass != nil) {
        
        NSDictionary * levelData = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:BUNDLE_FILE_PATH(level)] options:NSJSONReadingMutableContainers error:nil];
        
        BasicLevelController * levelController = [(BasicLevelController *)[levelClass alloc] initWithDictionary:levelData ? levelData:@{}];
        
        return [levelController autorelease];
    }
    
    return nil;
}

- (id)initWithDictionary:(NSDictionary *)levelDictionary {
    
    if(self = [super init]) {
        
#warning TODO: Improve!
        
        /* Does not check for validity! */
        
        _mapSize = [(NSNumber *)levelDictionary[@"size"] unsignedIntValue];
        
        ceilMap = GMCreateWithDictionary(@{@"mapSize":@(_mapSize), @"matrix":levelDictionary[@"ceiling"]});
        NSArray * ceilingObjects = (NSArray *)levelDictionary[@"ceilingObjects"];
        NSMutableArray * ceilings = [NSMutableArray array];
        
        for(NSDictionary * ceiling in ceilingObjects) {
            
            [ceilings addObject:[[[MapCeiling alloc] initWithDictionary:ceiling] autorelease]];
        }
        
        _ceilingKinds = [[NSArray arrayWithArray:ceilings] retain];
        
        objectMap = GMCreateWithDictionary(@{@"mapSize":@(_mapSize), @"matrix":levelDictionary[@"map"]});
        
        NSArray * mapObjects = (NSArray *)levelDictionary[@"mapObjects"];
        NSMutableArray * mObjects = [NSMutableArray array];
        
        for(NSDictionary * mObject in mapObjects) {
            
            [mObjects addObject:[[[MapObject alloc] initWithDictionary:mObject] autorelease]];
        }
        
        _objectKinds = [[NSArray arrayWithArray:mObjects] retain];
        
        floorMap = GMCreateWithDictionary(@{@"mapSize":@(_mapSize), @"matrix":levelDictionary[@"floor"]});
        
        NSArray * floorObjects = (NSArray *)levelDictionary[@"floorObjects"];
        NSMutableArray * floors = [NSMutableArray array];
        
        for(NSDictionary * floor in floorObjects) {
            
            [floors addObject:[[[MapFloor alloc] initWithDictionary:floor] autorelease]];
        }
        
        _floorKinds = [[NSArray arrayWithArray:floors] retain];
        
        NSDictionary * initial = levelDictionary[@"initial"];
        
        _spawnAngle = [(NSNumber *)initial[@"angle"] doubleValue];
        spawnGridX = [(NSNumber *)initial[@"x"] unsignedIntValue];
        spawnGridY = [(NSNumber *)initial[@"y"] unsignedIntValue];
        
        if(levelDictionary[@"extra"] != nil && [levelDictionary[@"extra"] isKindOfClass:[NSDictionary class]]) {
            
            _extraData = [levelDictionary[@"extra"] retain];
        }
        
        if(levelDictionary[@"skybox"] != nil && ![(NSString *)levelDictionary[@"skybox"] isEqualToString:@""]) {
            
            _skybox = [[Texture alloc] init];
            [_skybox setImage:levelDictionary[@"skybox"]];
        }
        
        if(levelDictionary[@"audio"] != nil && ![(NSString *)levelDictionary[@"audio"] isEqualToString:@""]) {
            
            _globalAudio = [[Audio alloc] init];
            [_globalAudio setAudio:levelDictionary[@"audio"]];
            [_globalAudio setLoops:YES];
            [_globalAudio setStereo:YES];
        }
    }
    
    return self;
}

#pragma mark - Map data

/* Returning the map... The receiver can alter the map contents, 
 * though it is not encouraged.
 */

- (unsigned)mapSize {
    
    return _mapSize;
}

- (GameMap *)ceiling {
    
    return ceilMap;
}

- (GameMap *)objects {
    
    return objectMap;
}

- (GameMap *)floor {
    
    return floorMap;
}

- (Vector3)spawnPosition {
    
    return newVector(spawnGridX * GRID_LENGTH + GRID_LENGTH/2.0, spawnGridY * GRID_LENGTH + GRID_LENGTH/2.0, 0);
}

- (Angle)spawnAngle {
    
    return _spawnAngle;
}

- (double)maxDistance {
    
    return (GRID_LENGTH * _mapSize * sqrt(2.0));
}

- (MapCeiling *)ceilingAt:(unsigned)index {
    
    if(index == 0) return nil;
    
    if(index - 1 < [self.ceilingKinds count]) {
        
        return self.ceilingKinds[index - 1];
    }
    
    return nil;
}

- (unsigned)countOfCeilings {
    
    return [self.ceilingKinds count];
}

- (MapObject *)objectAt:(unsigned)index {
    
    if(index == 0) return nil;
    
    if(index - 1 < [self.objectKinds count]) {
        
        return self.objectKinds[index - 1];
    }
    
    return nil;
}

- (unsigned)countOfObjects {
    
    return [self.objectKinds count];
}

- (MapFloor *)floorAt:(unsigned)index {
    
    if(index == 0) return nil;
    
    if(index - 1 < [self.floorKinds count]) {
        
        return self.floorKinds[index - 1];
    }
        
    return nil;
}

- (unsigned)countOfFloors {
    
    return [self.floorKinds count];
}

#pragma mark - Actions

- (void)shootActionAt:(unsigned)index {

    MapObject * obj = [self objectAt:index];
    
    if([obj hasShootAction]) {
        
        SEL action = NSSelectorFromString(obj.shootAction);
        
        [self performSelector:action];
    }
}

- (void)activateActionAt:(unsigned)index {
    
    MapObject * obj = [self objectAt:index];
    
    if([obj hasActivateAction]) {
        
        SEL action = NSSelectorFromString(obj.activateAction);
        
        [self performSelector:action];
    }
}

- (void)collisionActionAt:(unsigned)index {
    
    MapObject * obj = [self objectAt:index];
    
    if([obj hasCollisionAction]) {
        
        SEL action = NSSelectorFromString(obj.collisionAction);
        
        [self performSelector:action];
    }
}

- (void)stepOverActionAt:(unsigned)index {
    
    MapFloor * obj = [self floorAt:index];
    
    if([obj hasStepOverAction]) {
        
        SEL action = NSSelectorFromString(obj.stepOverAction);
        
        [self performSelector:action];
    }
}

#pragma mark - Aux

- (void)replaceObjectOfKind:(unsigned)o withKind:(unsigned)n {
    
    GameMap * map = [self objects];
    unsigned mapSize = [self mapSize];
    
    for(unsigned x = 0; x < mapSize; x++) {
        
        for(unsigned y = 0; y < mapSize; y++) {
            
            if(GMItemAtPosition(map, x, y) == o) {
                
                GMSetItemAtPosition(map, x, y, n);
            }
        }
    }
}

#pragma mark - Deinit

- (void)dealloc {
    
    GMReleaseMap(&ceilMap);
    GMReleaseMap(&objectMap);
    GMReleaseMap(&floorMap);
    
    [_skybox release], _skybox = nil;
    [_globalAudio release], _globalAudio = nil;
    [_extraData release], _extraData = nil;
    
    [_ceilingKinds release], _ceilingKinds = nil;
    [_objectKinds release], _objectKinds = nil;
    [_floorKinds release], _floorKinds = nil;
    
    [super dealloc];
}

#pragma mark - Override

- (void)levelDidLoad:(NSString *)previousLevel {
    
}

- (void)levelDidLoadFromState:(NSDictionary *)state {
    
}

- (void)levelUpdate:(unsigned)heartbeat {
    
}

- (void)levelWillUnload {
    
}

- (BOOL)levelAllowsSaving {
    return NO;
}

- (NSDictionary *)levelSaveState {
    
#warning TODO
    
    return @{};
}

- (BOOL)levelAllowsPlayerMovement {
    return YES;
}

@end
