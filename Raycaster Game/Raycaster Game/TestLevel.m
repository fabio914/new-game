//
//  TestLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/26/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "TestLevel.h"
#import "Game.h"

@interface TestLevel()

@property (nonatomic) BOOL didGiveHint;
@property (nonatomic) BOOL switchState;

@end

@implementation TestLevel

- (void)levelDidLoad:(NSString *)previousLevel {
    
    _switchState = [self.extraData[@"switchInitialState"] boolValue];
    
    [[[self objectAt:4] texture] setCurrentTexture:_switchState ? 0:1];
    [[[self objectAt:3] path] setActive:_switchState];
}

- (void)levelUpdate:(unsigned int)heartbeat {
    
}

- (GameAction)hint {
    
    if(!_didGiveHint) {
     
        [[Game shared] presentInfo:@"Be careful..." withDuration:2.0];
        _didGiveHint = YES;
    }
}

- (GameAction)switchChange {
    
    _switchState = !_switchState;

    [[[self objectAt:4] texture] setCurrentTexture:_switchState ? 0:1];
    
    [[Game shared] presentInfo:[NSString stringWithFormat:@"Switch is %@", _switchState ? @"on":@"off"] withDuration:2.0];
    
    [[[self objectAt:3] path] setActive:_switchState];
}

- (GameAction)killPlayer {
    
    [[Game shared] reloadLevel];
}

- (GameAction)nextLevel {
    
    if(_switchState) {
        
        [[Game shared] presentInfo:@"You better turn that thing off before you go..." withDuration:2.0];
    }
    
    else {
    
        [[Game shared] loadNextLevel];
    }
}

@end
