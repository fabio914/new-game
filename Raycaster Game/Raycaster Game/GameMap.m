//
//  GameMap.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#include "GameMap.h"

NSDictionary * GMDictionaryRepresentation(GameMap * map) {
    
    if(map != NULL && map->matrix != NULL) {
    
        NSMutableDictionary * representation = [NSMutableDictionary dictionary];
        
        representation[@"mapSize"] = [NSNumber numberWithUnsignedInt:map->mapSize];
        
        NSMutableArray * matrix = [NSMutableArray array];
        
        for(unsigned i = 0; i < map->mapSize; i++) {
            
            matrix[i] = [NSMutableArray array];
            
            for(unsigned j = 0; j < map->mapSize; j++) {
                
                matrix[i][j] = [NSNumber numberWithUnsignedChar:map->matrix[i][j]];
            }
        }
        
        representation[@"matrix"] = matrix;
        
        return [NSDictionary dictionaryWithDictionary:representation];
    }
    
    return @{};
}

GameMap * GMCreateWithDictionary(NSDictionary * dictionary) {
    
    GameMap * ret = NULL;
    
    /* Does not check for dictionary integrity */
    
    unsigned mapSize = [(NSNumber *)dictionary[@"mapSize"] unsignedIntValue];
    
    if((ret = GMCreateMap(mapSize)) != NULL) {
        
        NSArray * matrix = dictionary[@"matrix"];
        
        for(unsigned i = 0; i < mapSize; i++) {
            
            NSArray * line = matrix[i];
            
            for(unsigned j = 0; j < mapSize; j++) {
                
                GMSetItemAtPosition(ret, j, i, [(NSNumber *)line[j] unsignedCharValue]);
            }
        }
    }
    
    return ret;
}
