//
//  AppDelegate.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/19/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (retain, nonatomic) UIWindow *window;

@end
