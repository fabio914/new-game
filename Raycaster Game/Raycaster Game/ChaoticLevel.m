//
//  ChaoticLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/3/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "ChaoticLevel.h"
#import "Game.h"

@interface ChaoticLevel()

@property (nonatomic) BOOL switchState;

@end

@implementation ChaoticLevel

- (void)levelDidLoad:(NSString *)previousLevel {
    
    _switchState = NO;
    
    [[[self objectAt:9] texture] setCurrentTexture:_switchState ? 0:1];
    [[[self floorAt:9] path] setActive:_switchState];
}

- (GameAction)switchChange {
    
    if(!_switchState) {
        
        _switchState = YES;
     
        [[[self objectAt:9] texture] setCurrentTexture:_switchState ? 0:1];
        [[[self floorAt:9] path] setActive:_switchState];
    }
}

- (GameAction)killPlayer {
    
    [[Game shared] reloadLevel];
}

- (GameAction)nextLevel {
    
    [[Game shared] loadNextLevel];
}

@end
