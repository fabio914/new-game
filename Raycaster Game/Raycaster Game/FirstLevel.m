//
//  FirstLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "FirstLevel.h"
#import "Game.h"

@interface FirstLevel() {

    BOOL startedTestAction;
    BOOL finishedTestAction;
    unsigned testActionCounter;
}

@end

@implementation FirstLevel

- (void)levelUpdate:(unsigned int)heartbeat {
    
    if(startedTestAction && !finishedTestAction) {
    
        /* Update once every second */
        if(heartbeat % HEARTBEATS_PER_SEC == 0) {
            
            if(testActionCounter < 3) {
            
                unsigned x = 3 + (2 * testActionCounter);
                
                GMSetItemAtPosition(self.objects, x, 3, 2);
                GMSetItemAtPosition(self.objects, x, 7, 2);
                testActionCounter++;
            }
            
            else {
                
                finishedTestAction = YES;
                [[Game shared] presentInfo:@"Done" withDuration:2.f];
            }
        }
    }
}

- (BOOL)levelAllowsPlayerMovement {
    
    return (!startedTestAction || finishedTestAction);
}

- (GameAction)testAction {
    
    if(!startedTestAction) {
        
        [[Game shared] presentInfo:@"Test action!" withDuration:2.f];
        
        GMSetItemAtPosition(self.objects, 2, 4, 0);
        GMSetItemAtPosition(self.objects, 2, 6, 0);
        
        testActionCounter = 0;
        startedTestAction = YES;
    }
}

- (GameAction)nextLevel {
    
    [[Game shared] loadNextLevel];
}

@end
