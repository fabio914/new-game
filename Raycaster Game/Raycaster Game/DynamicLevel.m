//
//  DynamicLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/29/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "DynamicLevel.h"
#import "Game.h"

@interface DynamicLevel()

@property (retain, nonatomic) NSArray * yellowKeyPosition;
@property (nonatomic) BOOL hasYellowKey, switchOn;

@end

@implementation DynamicLevel

- (void)levelDidLoad:(NSString *)previousLevel {
    
    _yellowKeyPosition = [self.extraData[@"yellowKeyPosition"] retain];
    
    GMSetItemAtPosition(self.objects, [_yellowKeyPosition[0] unsignedIntValue], [_yellowKeyPosition[1] unsignedIntValue], 11);
    
    _hasYellowKey = NO;
    _switchOn = NO;
    [[[self objectAt:4] texture] setCurrentTexture:0];
}

- (GameAction)getYellowKey {
    
    GMSetItemAtPosition(self.objects, [_yellowKeyPosition[0] unsignedIntValue], [_yellowKeyPosition[1] unsignedIntValue], 0);
    
    [[Game shared] presentInfo:@"You got the yellow key!" withDuration:2.0];
    
    _hasYellowKey = YES;
}

- (GameAction)switchChange {
    
    if(_hasYellowKey) {
        
        if(!_switchOn) {
        
            [[[self floorAt:6] path] setActive:YES];
            [[[self objectAt:4] texture] setCurrentTexture:1];
            
            _switchOn = YES;
        }
    }
    
    else {
        
        [[Game shared] presentInfo:@"You need the yellow key to activate this item!" withDuration:2.f];
    }
}

- (GameAction)killPlayer {
    
    [[Game shared] reloadLevel];
}

- (GameAction)nextLevel {
    
    [[Game shared] loadNextLevel];
}

- (void)levelWillUnload {
    
    [_yellowKeyPosition release], _yellowKeyPosition = nil;
    [super levelWillUnload];
}

@end
