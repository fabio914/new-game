//
//  IntroLevel.m
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 7/18/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import "IntroLevel.h"
#import "Game.h"

@interface IntroLevel()

@property (nonatomic) BOOL didShowWelcome;

@end

@implementation IntroLevel

- (void)advanceFloorTextureOfItems:(unsigned)first, ... {
    
    va_list args;

    [[[self floorAt:first] texture] setCurrentTexture:1];
    
    va_start(args, first);
    unsigned item = va_arg(args, unsigned);
    
    while(item != 0) {
        
        [[[self floorAt:item] texture] setCurrentTexture:1];
        item = va_arg(args, unsigned);
    }

    va_end(args);
}

- (GameAction)activate {
    
    if(!_didShowWelcome) {
        
        [[Game shared] presentInfo:@"Welcome!" withDuration:2.f];
        
        [self replaceObjectOfKind:1 withKind:2];
        [self advanceFloorTextureOfItems:2, 3, 4, 5, 6, 7, 8, 9, 11, 0];
        
        _didShowWelcome = YES;
    }
}

- (GameAction)killPlayer {
    
    [[Game shared] reloadLevel];
}

- (GameAction)nextLevel {
    
    if(_didShowWelcome) {
    
        [[Game shared] loadNextLevel];
    }
}

@end
