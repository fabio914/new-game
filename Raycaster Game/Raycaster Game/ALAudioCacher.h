//
//  ALAudioCacher.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/28/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALAudioCacher : NSObject

@property (retain, nonatomic) NSMutableDictionary * loadedSounds;

+ (id)shared;
- (unsigned)loadSound:(NSString *)sound stereo:(BOOL)stereo;
- (void)unloadSounds;

@end
