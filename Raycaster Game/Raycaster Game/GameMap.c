//
//  GameMap.c
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/20/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#include "GameMap.h"

GameMap * GMCreateMap(unsigned mapSize) {
    
    GameMap * ret = NULL;
    
    if(mapSize != 0) {
        
        if((ret = (GameMap *)malloc(sizeof(GameMap))) != NULL) {
            
            ret->mapSize = mapSize;
            
            if((ret->matrix = (uint8_t **)malloc(mapSize * sizeof(uint8_t *))) != NULL) {
                
                unsigned i, j;
                
                for(i = 0; i < mapSize; i++) {
                    
                    if((ret->matrix[i] = (uint8_t *)malloc(mapSize * sizeof(uint8_t))) != NULL) {
                        
                        for(j = 0; j < mapSize; j++) {
                            
                            ret->matrix[i][j] = 0;
                        }
                    }
                    
                    else {
                        
                        for(j = 0; j < i; j++) {
                            
                            free(ret->matrix[j]);
                        }
                        
                        break;
                    }
                }
            }
            
            else {
                
                free(ret);
                ret = NULL;
            }
        }
    }
    
    return ret;
}

uint8_t GMItemAtPosition(GameMap * map, int gridX, int gridY) {
    
    if(map != NULL && map->matrix != NULL) {
        
        if((gridX >= 0 && gridX < map->mapSize) && (gridY >= 0 && gridY < map->mapSize)) {
            
            return map->matrix[gridY][gridX];
        }
    }
    
    return 0;
}

void GMSetItemAtPosition(GameMap * map, int gridX, int gridY, uint8_t value) {
    
    if(map != NULL && map->matrix != NULL) {
        
        if((gridX >= 0 && gridX < map->mapSize) && (gridY >= 0 && gridY < map->mapSize)) {
            
            map->matrix[gridY][gridX] = value;
        }
    }
}

void GMReleaseMap(GameMap ** map) {
    
    if(map != NULL && *map != NULL) {
        
        unsigned i;
        
        for(i = 0; i < (*map)->mapSize; i++) {
            
            free((*map)->matrix[i]);
        }
        
        free((*map)->matrix);
        free(*map);
        
        *map = NULL;
    }
}
