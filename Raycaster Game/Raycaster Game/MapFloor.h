//
//  MapFloor.h
//  Raycaster Game
//
//  Created by Fabio Dela Antonio on 6/21/14.
//  Copyright (c) 2014 Bluenose. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Texture.h"
#import "Path.h"

@interface MapFloor : NSObject

/* Textures */
@property (retain, nonatomic) AnimatedTexture * texture;

/*
 * ATTENTION: You cannot have more than one copy of
 * a floor if it has a path.
 */

@property (retain, nonatomic) Path * path;

/* Actions */
@property (retain, nonatomic) NSString * stepOverAction;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (BOOL)hasTexture;
- (BOOL)hasPath;
- (BOOL)hasStepOverAction;

@end
